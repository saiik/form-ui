/**
 * Forms Class Object
 * 
 * @version 0.0.7 DEV
 */

"use strict";

/**
 * do some main stuff here
 *
 * to get some widths and positions
 **/
jQuery(document).ready(function(){ 
	window.gridPos = $("#formBuildGrid").position();
	window.gridWidth = $("#formBuildGrid").outerWidth();
	window.gridHeight = $("#formBuildGrid").outerHeight();
	window.gridCompleteWidth = gridPos.left + gridHeight;
	window.left = gridPos.left - 0.5;
	window.windowHeight = window.innerHeight;
	window.windowWidth = window.innerWidth;
});

/**
 * initialize form class
 * 
 * @return object
 */
var $formBuilder = function(w) {

	if(typeof(w) == "undefined" || w == "" || typeof(w) !== "object") {
		throw new Error("FormBuilder requires a window");
	}

	if(typeof(jQuery) == "undefined") {
		throw new Error("FormBuilder requires jQuery > v1.8 / v2.x");
	}

	if(typeof(angular) == "undefined") {
		throw new Error("FormBuilder requires AngularJS > v1.2");
	}

	this.config = {
		version: '0.0.8 DEV',
		build: '00800',
		date: '10-09-2014',
		name: 'eLogForms',
		codeName: 'Dragon',
		debug: false,
		details: {
				author: 'Tobias Fuchs', 
				email: 'tobias.f@elogbooks.co.uk',
				company: 'Elogbooks Facility Management Ltd'
			},
		tableConfig: {
			tableWidth: 720,
			assetWidth: 715,
			custom: {
				maxRows: 20,
				maxCols: 7
			},
			asset: {
				maxRows: -1,
				maxCols: 7
			}
		},
		autoSaveOnOff: true,
		autosave: 120000
	};

	this.collision = false;

	this.window = w;

	console.log("FormBuilder Version " + this.config.version + " loaded");

	return this;
}


/**
 * cache for the box's
 * 
 * @type {Object}
 */
var boxes = {box: []};

/**
 * number of the box
 * 
 * @type {Number}
 */
var boxesCount = 0;

/**
 * cache for the containers
 *
 * @type {Array}
 */
var containers = [];

/**
 * current z-index
 *
 * @type {Number}
 */
var zIndex = 10;

/**
 * cache for table rows
 * 
 * @type {Array}
 */
var tableInformationRows = [];

/**
 * cache for table cols
 * @type {Array}
 */
var tableInformationCols = [];

/**
 * current alert number
 * 
 * @type {Number}
 */
var alert_ = 0;

/**
 * initialize some important stuff
 * 
 * 
 */
$(document).ready(function() {
	if(form.config.debug === true) {
		var debugHandle = "<div class=\"formWidgetContainer\">" + 
			"<div class=\"formWidgetContainerHead\">" + 
				"Debug: " +
			"</div>" + 
			"<div class=\"formWidgetContainerContent debug\">" + 
			"</div>" +
			"</div>";
		$(debugHandle).appendTo(".widgets");
	}

	$(".ab").draggable({
		grid: [ 20, 20 ],
		revert: true
	});

	$(".im").draggable({
		grid: [ 20, 20 ],
		revert: true
	});

	$(".ab").click(function(e) {
		e.preventDefault;

		form.createWhiteBox(0,0,true);
	});

	$(".im").click(function(e) {
		e.preventDefault;

		var scope = angular.element($("#formWrapper")).scope();
			scope.getImages();
		scope.safeApply(function() {
			scope.windowTpl = "img";
			scope.showWindow = true;
		});
	});

	$("#formBuildGrid").droppable({
		drop: function(event, ui) { form.dropFunction(event, ui); }
	});

	$("#trashBin").droppable({
		tolerance: 'pointer',
		drop: function(event, ui) { form.deleteBox(event, ui); }
	});
	$("body").keypress(function(e) {
		if(e.keyCode == 27) {
			var scope = angular.element($("#formWrapper")).scope();
			scope.safeApply(function() {
				scope.showWindow = false;
			});
			var currentLocation = window.location;
				currentLocation.hash;
			if(currentLocation.hash == "#/preview/" || currentLocation.hash == "#/menu/") {		
				window.location = '#new';
			}
		}
	});
});

$(document).bind("click", function(event) {
	$("#contextMenu").hide();
	$("#contextMenuHead").hide();
});

/**
 * function dropFunction
 * handles the drop event of an droppable object
 * 
 * @param  obj event
 * @param  obj ui
 * @return bool
 */
$formBuilder.prototype.dropFunction = function(event, ui) {
	if(ui.draggable.context.id == "ab") {
		form.createWhiteBox(event, ui);
	}
	if(ui.draggable.context.id == "im") {
		var scope = angular.element($("#formWrapper")).scope();
		scope.safeApply(function() {
			scope.event = event;
			scope.ui = ui;
			scope.windowTpl = "img";
			scope.showWindow = true;
		});
	}
}

/**
 * function createWhiteBox
 * creates a new whitebox in the gridbox
 * 
 * @param  obj event
 * @param  obj ui
 * @return bool
 */
$formBuilder.prototype.createWhiteBox = function(event, ui, click, pos_x, pos_y, width, height, id, content, sqlId) {
	if(click === true) {
		var top = 0;
		$('#formBuildGrid').children().each(function() {
			var self = $(this);
				top = Math.max(top, self.position().top);
		});
			top = top + 60;
		var left = 20;

	} else {
		var top = ui.offset.top - 70;
		var left = ui.offset.left - gridWidth + 40;
	}

	if(top > 740) {
		var height_ = $("#formBuildGrid").height();
			height_ = height_ + 60;

		var css_ = {
			height: height_ + "px"
		};

		$("#formBuildGrid").css(css_);
	}

	var css = {
		top: (typeof(pos_y) != "undefined"?pos_y:top),
		left: (typeof(pos_x) != "undefined"?pos_x:left),
		width: (typeof(width) != "undefined"?width:380),
		minHeight: (typeof(height) != "undefined"?height+"px":40),
		background: '#fff',
		border: '1px #ccc solid',
		cursor: 'move',
		zIndex: (typeof(pos_y) != "undefined"?'':zIndex),
		padding: 10,
		position: (typeof(pos_y) != "undefined"?'absolute':'absolute')
	};
		
	var boxDiv = '<div id="' + (typeof(id) != "undefined"?id:'whiteBox-' + boxesCount.toString()) + '" data-type="whitebox" ' + (typeof(sqlId) != "undefined"?'data-information="'+sqlId+'"':'') + ' class="whitebox">' + (form.config.debug?'whiteBox-' + boxesCount.toString():'') + 
					'<div class="whiteBoxContent">'+(typeof(content) != "undefined"?content:'')+'</div>' + 
				  '</div>';

	var addBox = $(boxDiv).css(css).appendTo($('#formBuildGrid'));
	var self = (typeof(id) != "undefined"?"#"+id:'#whiteBox-' + boxesCount);
	if(typeof(id) == "undefined") {
		var selfId = boxesCount;
		boxesCount = boxesCount+1;
		zIndex = zIndex +1;
	} else {
		var getId = id.split("-");
		var selfId = getId[1];
		boxesCount = parseInt(getId[1]) + 1;
	}

	$(self)
	.draggable({ 
		grid: [ 20, 20 ], 
		containment: "#formBuildGrid", 
		scroll: false,
		drag: function(event,ui) { form.handleDrag(event, ui, $(this)); },
		stop: function(event,ui) { form.handleStop(event, ui, $(this)); }
	})
	.resizable({ 
		grid: 20, 
		containment: "#formBuildGrid", 
		minWidth: 160, 
		minHeight: 20,
		maxWidth: 800,
		maxHeight: 800,
		stop: function(event, ui) { ui.element.height('auto'); form.handleStopResize(event, ui); },
		autoHide: true
	});

	$(self).dblclick(function() {
		var scope = angular.element($("#formWrapper")).scope();
		var content = $(self + " .whiteBoxContent").html();
			content = content.replace(/<br>/g, "\r\n");

		scope.$apply(function() {
			scope.windowTpl = 'whitebox';
			scope.showWindow = true;
			scope.contentId = self;
			scope.content = content;

			var wWidth  = ($("#windowBoxes").outerWidth()/2);
			var wHeight = ($("#windowBoxes").outerHeight()/2);

			$("#windowBoxes").css({
				marginTop: '-' + wHeight + 'px',
				marginLeft: '-' + wWidth + 'px'
			});
		});
	});

	form.devLog("Created: " + self);

	return true;
}

/**
 * function saveWhiteContent
 * saves content to a whitebox
 * 
 * @param  string content
 * @param  int	  id
 * @return bool
 */
$formBuilder.prototype.saveWhiteContent = function(content, id) {
	if(typeof(content) === "undefined") return false;
	if(typeof(id) === "undefined") return false;

	var entityMap = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': '&quot;',
		"'": '&#39;',
		"/": '&#x2F;'
	};

	function escapeHtml(string) {
		return String(string).replace(/[&<>\/]/g, function (s) {
			return entityMap[s];
		});
 	}

	var div_id = id;
		content = escapeHtml(content);
		content = content.replace(/\r\n/g, "<br />").replace(/\n/g, "<br />");


	if(div_id.indexOf(";") > -1) {
		div_id = div_id.split(";");
		if(div_id[0] == ".tablehead") {
			content = content.replace("{text}", "{input}").replace("{checkbox}", "{check}");
		}
		$(div_id[1] + " " + div_id[0], div_id[2]).html(content)
	} else {
		$(id + " .whiteBoxContent").html(content);
	}
	return true;
}

/**
 * function createImageBox
 * creates an image
 * 
 * @param  string imgUrl
 * @return bool
 */
$formBuilder.prototype.createImageBox = function(imgUrl, event, ui, click, div_id, img_id, pos_x, pos_y, width, height, sqlId) {
	if(typeof(imgUrl) === "string") {
		imgUrl = imgUrl.split(".");
		if(typeof(imgUrl[2]) != "undefined") {
			imgUrl = imgUrl[0]+"."+imgUrl[2];
		} else {
			imgUrl = imgUrl[0]+"."+imgUrl[1];
		}

		if(click === true) {
			var top = 0;
			$('#formBuildGrid').children().each(function() {
				var self = $(this);
					top = Math.max(top, self.position().top);
			});
				top = top + 60;
			var left = 20;
		} else {
			var top = ui.offset.top - 70;
			var left = ui.offset.left - gridWidth + 40;
		}

		if(top > 740) {
			var height_ = $("#formBuildGrid").height();
				height_ = height_ + 60;

			var css_ = {
				height: height_ + "px"
			};

			$("#formBuildGrid").css(css_);
		}

		var css = {
			top: (typeof(pos_y) != "undefined"?pos_y:top),
			left: (typeof(pos_x) != "undefined"?pos_x:left),
			width: (typeof(width) != "undefined"?width:380),
			height: (typeof(height) != "undefined"?height:120),
			cursor: 'move',
			zIndex: (typeof(pos_y) != "undefined"?'':zIndex),
			position: (typeof(pos_y) != "undefined"?'absolute':'absolute')
		};

		var imgBox = '<div id="' + (typeof(div_id) != "undefined"?div_id:'imageBox-' + boxesCount.toString() + '') + '" style="display: inline-block; border: 1px #000 solid;"></div>';
		var img = '<img id="' + (typeof(div_id) != "undefined"?img_id:'image-' + boxesCount.toString() + '') + '" data-type="image" src="'+imgUrl+'" class="resizable test" alt="" title=""/>';
		var addImg = $(img).css(css).appendTo($('#formBuildGrid'));

		var self = (typeof(img_id) != "undefined"?"#"+img_id:"#image-" + boxesCount);

		$(self)
		.resizable({ 
			grid: 20, 
			containment: "#formBuildGrid", 
			minWidth: 160, 
			minHeight: 80,
			width: 'auto',
			height: 'auto',
			handles: 'all',
			maxWidth: 800,
			maxHeight: 400,
			stop: function(event, ui) { form.handleStopResize(event, ui)}
			})
		.parent()
		.draggable({ 
			grid: [ 20, 20 ], 
			containment: "#formBuildGrid", 
			scroll: false,
			drag: function(event,ui) { form.handleDrag(event, ui, $(self)); },
			stop: function(event,ui) { form.handleStop(event, ui, $(self)); },
		});
		if(typeof(img_id) != "undefined") {
			$("#" + img_id).parent().attr("id", div_id).attr("data-type", "image").attr("data-information", sqlId);
		} else {
			$("#image-" + boxesCount).parent().attr("id", "imageBox-" + boxesCount).attr("data-type", "image");
		}

		boxesCount = boxesCount+1;
		zIndex = zIndex +1;

		return true;
	} else {
		return false;
	}
}

/**
 * function createNewVariable
 * creates a new box as a $variable
 * 
 * @param  string input
 * @return bool
 */
$formBuilder.prototype.createNewVariable = function(input, top, left) {
	if(typeof(input) === "string") {
		if(typeof(top) == "undefined") {
			var top = 0;
			$('#formBuildGrid').children().each(function() {
				var self = $(this);
					top = Math.max(top, self.position().top);
			});
				top = top + 60;
		} else {
			var top = top;
		}
		if(typeof(left) == "undefined") {
			var left = 20;
		} else {
			var left = left;
		}

		if(top > 740) {
			var height_ = $("#formBuildGrid").height();
				height_ = height_ + 60;

			var css_ = {
				height: height_ + "px"
			};

			$("#formBuildGrid").css(css_);
		}

		var css = {
			top: top,
			left: left,
			minWidth: 40,
			height: 20,
			cursor: 'move',
			position: 'absolute',
			zIndex: zIndex
		};

		var addVariable = '<span id="var-' + boxesCount.toString() + '" data-type="variable">' + input + '</span>';

		var addBox = $(addVariable).css(css).appendTo($('#formBuildGrid'));
		// $("#var-" + boxesCount).parent().attr("data-type", "variable").attr("id", "variableBox-" + boxesCount);
		$("#var-" + boxesCount)
		.draggable({ 
			grid: [ 20, 20 ], 
			containment: "#formBuildGrid", 
			scroll: false,
			drag: function(event,ui) { form.handleDrag(event, ui, $(this)); },
			stop: function(event,ui) { form.handleStop(event, ui, $(this)); },
		});

		boxesCount = boxesCount+1;
		zIndex = zIndex + 1;

		return true;
	} else {
		return false;
	}
}

/**
 * creates an asset table
 * 
 * @param  object	asset
 * @return boolean
 */
$formBuilder.prototype.createAssetTable = function(asset) {

	var top = 0;
	$('#formBuildGrid').children().each(function() {
		var self = $(this);
			top = Math.max(top, self.position().top);
	});
		top = top + 60;
	var left = 20;

	if(top > 740) {
		var height_ = $("#formBuildGrid").height();
			height_ = height_ + 60;

		var css_ = {
			height: height_ + "px"
		};

		$("#formBuildGrid").css(css_);
	}

	var css = {
		top: top,
		left: left,
		width: 760,
		minHeight: 20,
		position: 'absolute',
		backgroundColor: '#fff',
		zIndex: zIndex
	};

	var length = 0;
	var o = [];

	$.map(asset, function(value, index) {
		if(value === true) {
			length++;
		}
	});

	var cols = length;
	
	if(cols === 0) {
		form.alertMessages("danger", "Error", "Can't create empty table");
		return false;
	}

	var newWidth = Math.floor((form.config.tableConfig.assetWidth/cols));

	var table = '<div id="assetTable-' + boxesCount.toString() + '" class="table" data-type="assetTable">';
		table = table + '<div class="tableName">SubAssets</div>';
		table = table + '<table class="main"><thead><tr>';
		table = table + '<td><table class="child idTable table-0"><thead><tr><td class="tablehead" style="width: 45px;">ID</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>'
	
	var t_ = 1;

	$.map(asset, function(value, index) {
		switch(index) {
			case "status":
				if(value === true) {
					table = table + '<td data-asset="status"><table class="child nonId tab-'+t_+'"><thead><tr><td style="width: '+newWidth+'px;" class="tablehead th-1 noId">{stt}</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>';
				}
			break;
			case "model":
				if(value === true) {
					table = table + '<td data-asset="model"><table class="child nonId tab-'+t_+'"><thead><tr><td style="width: '+newWidth+'px" class="tablehead th-2 noId">{model}</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>';
				}
			break;
			case "serial":
				if(value === true) {
					table = table + '<td data-asset="serial"><table class="child nonId tab-'+t_+'"><thead><tr><td style="width: '+newWidth+'px" class="tablehead th-4 noId">{serialno}</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>';
				}
			break;
			case "notes":
				if(value === true) {
					table = table + '<td data-asset="notes"><table class="child nonId tab-'+t_+'"><thead><tr><td style="width: '+newWidth+'px" class="tablehead th-4 noId">{notes}</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>';
				}
			break;
			case "name":
				if(value === true) {
					table = table + '<td data-asset="name"><table class="child nonId tab-'+t_+'"><thead><tr><td style="width: '+newWidth+'px" class="tablehead th-5 noId">{sname}</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>';
				}
			break;
			case "desc":
				if(value === true) {
					table = table + '<td data-asset="desc"><table class="child nonId tab-'+t_+'"><thead><tr><td style="width: '+newWidth+'px" class="tablehead th-6 noId">{sdescr}</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>';
				}
			break;
			case "loca":
				if(value === true) {
					table = table + '<td data-asset="loca"><table class="child nonId tab-'+t_+'"><thead><tr><td style="width: '+newWidth+'px" class="tablehead th-7 noId">{loc}</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>';
				}
			break;
			case "input":
				if(value === true) {
					table = table + '<td data-asset="input"><table class="child nonId tab-'+t_+'"><thead><tr><td style="width: '+newWidth+'px" class="tablehead th-7 noId">{input}</td></tr></thead><tbody><tr><td style="display: none;"/</td></tr></tbody></table></td>';
				}
			break;
			case "check":
				if(value === true) {
					table = table + '<td data-asset="check"><table class="child nonId tab-'+t_+'"><thead><tr><td style="width: '+newWidth+'px" class="tablehead th-7 noId">{check}</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>';
				}
			break;
		}

		t_++;
	});

		table = table + '</tr></thead></table>';
		table = table + '<div class="editAssetTable" onclick="form.openAssetTable(\'#assetTable-' + boxesCount.toString() + '\')"><span class="glyphicon glyphicon-pencil"></span></div>';
		table = table + '</div>';

	var addTable = $(table).css(css).appendTo("#formBuildGrid");

	$("table > thead > tr", "#assetTable-" + boxesCount).sortable({ items: "> td:not(:first)" });
	$("table > thead > tr > .noId", "#assetTable-" + boxesCount).resizable({
		handles: 'e, w',
		autoHide: true
	});
	$("table > thead > tr", "#assetTable-" + boxesCount).disableSelection();

	$("#assetTable-" + boxesCount)
	.draggable({ 
		grid: [ 20, 20 ], 
		containment: "#formBuildGrid", 
		scroll: false,
		drag: function(event,ui) { form.handleDrag(event, ui, $(this)); },
		stop: function(event,ui) { form.handleStop(event, ui, $(this)); },
	});

	boxesCount = boxesCount + 1;
	zIndex = zIndex + 1;

	return true;
}

/**
 * opens a new window to select and deselect the asset table fields
 * 
 * @param  int	  id
 * @return boolean
 */
$formBuilder.prototype.openAssetTable = function(id) {
	if(typeof(id) != "undefined") {
		var assets = {status: false, model: false, serial: false, notes: false, name: false, desc: false, loca: false, input: false, check: false};
		$(".main > thead > tr", id).children().each(function() {
			var type = $(this).data("asset");
			if(typeof(type) != "undefined") {
				switch(type) {
					case "status":
						assets.status = true;
					break;
					case "model":
						assets.model = true;
					break;
					case "serial":
						assets.serial = true;
					break;
					case "notes":
						assets.notes = true;
					break;
					case "name":
						assets.name = true;
					break;
					case "desc":
						assets.desc = true;
					break;
					case "loca":
						assets.loca = true;
					break;
					case "input":
						assets.input = true;
					break;
					case "check":
						assets.check = true;
					break;
					default:
						return false;
					break;
				}
			}
		});
		var scope = angular.element($("#formWrapper")).scope();
		scope.safeApply(function() {
			scope.windowTpl = "assettable";
			scope.showWindow = true;
			scope.assetId = id;
			scope.assetsCheck = assets;
		});
	} else {
		return false;
	}
}

/**
 * updates the asset table
 * 
 * @param  object   assets   
 * @param  int	    assetId 
 * @return boolean  
 */
$formBuilder.prototype.updateAssetTable = function(assets, assetId) {
	if(assets.length === 0) {
		form.alertMessages("danger", "Error", "Can't create empty table");
		return false;
	} else {
		var assetType = {status: 'stt', model: 'model', serial: 'serialno', notes: 'notes', name: 'sname', desc: 'sdescr', loca: 'loc', input: 'input', check: 'check'};

		var length = 0;
		var val = 0;
		$.map(assets, function(value, index) {
			if(value === false) {
				val++;
			}
			length++;
		});

		if(val === length) {
			return false;
		}

		$.map(assets, function(value, index) {
			if(value === false) {
				var count = 0;
				$(".main > thead > tr", assetId).children().each(function() {
					var type = $(this).data("asset");
					if(type == index) {
						$(this).remove();
						}
						count++;
					});

					count = count - 2;
					var width = Math.floor((form.config.tableConfig.assetWidth/count));
					$(".main > thead > tr", assetId).children().each(function() {
						var type = $(this).data("asset");
						if(typeof(type) != "undefined") {
							$(this).css({width: width+'px'});
							$(this).children().each(function() {
								$(this).css({width: width+'px'});
							});
						}
					});
			} else {
				if(value === true) {
					var count = 0;
					var match = false;
					$(".main > thead > tr", assetId).children().each(function() {
						var type = $(this).data("asset");
						if(type == index) {
							match = true;
						}
						count++;
					});
					if(match === false) {
						var width = Math.floor((form.config.tableConfig.assetWidth/count));
						var	table = '<td data-asset="'+index+'"><table class="child nonId tab-1"><thead><tr><td style="width: '+width+'px;" class="tablehead th-1 noId">{'+assetType[index]+'}</td></tr></thead><tbody><tr><td style="display: none;">/</td></tr></tbody></table></td>';
						$(".main > thead > tr", assetId).append(table);
						$(".main > thead > tr", assetId).children().each(function() {
							var type = $(this).data("asset");
							if(typeof(type) != "undefined") {
								$(this).css({width: width+'px'});
								$(this).children().each(function() {
									$(this).css({width: width+'px'});
								});
							}
						});
					}
				}
			}
		});
	}
}

/**
 * creates a table based and user defined rows and cols
 * 
 * @param  string name
 * @param  int	  cols
 * @param  int 	  rows
 * @return bool
 */
$formBuilder.prototype.createNewTable = function(name, cols, rows) {

	if(typeof(rows) === "string" && parseInt(cols) < (form.config.tableConfig.custom.maxCols + 1)) {
		if(typeof(rows) === "string" && (parseInt(rows) > 0 && parseInt(rows) < (form.config.tableConfig.custom.maxRows + 1))) {

			var top = 0;
			$('#formBuildGrid').children().each(function() {
				var self = $(this);
					top = Math.max(top, self.position().top, self.height());
			});
				top = top + 20;
				top = Math.round(top / 20) * 20;
			var left = 20;

			if(top > 740) {
				var height_ = $("#formBuildGrid").height();
					height_ = height_ + 60;

				var css_ = {
					height: height_ + "px"
				};

				$("#formBuildGrid").css(css_);
			}

			var css = {
				top: top,
				left: left,
				width: 760,
				minHeight: 20,
				backgroundColor: '#fff',
				position: 'absolute',
				zIndex: zIndex
			};

			var table = '<div id="table-' + boxesCount.toString() + '" class="table" data-type="customTable">';
				table = table + '<div class="tableName" style="cursor: move;">'+name+'</div>';

			var col = parseInt(cols) + 1;
			var row = parseInt(rows);
			var coln = col;

			var newWidth = Math.floor((form.config.tableConfig.tableWidth/cols));

			var collumData = {cols: cols, width: newWidth};

			var collumArray = [];

			table = table + '<table class="main">';
			table = table + '<tr>';
			for(var i = 0; i < col; i++) {

				table = table + '<td>';
				table = table + '<table class="child '+(i===0?'idTable':'nonId table-'+i)+'"><thead><tr>';
				table = table + '<td '+(i!==0?'oncontextmenu="form.headMenu(\''+i+'\', \'#table-' + boxesCount.toString() + '\')"':'')+' '+(i!==0?'ondblclick="form.editHead(\''+i+'\', \'#table-' + boxesCount.toString() + '\')"':'')+' class="tablehead" style="width: '+(i===0?'40px':newWidth)+';">'+(i===0?'ID':'')+'</td>';
				table = table + '</tr></thead><tbody>';

				for(var c = 0; c < row; c++) { 
					var bg = (c%2?'#fafafa':'#efefef');
					var idCount = c + 1;
					
					table = table + '<tr><td '+(i!==0?'oncontextmenu="form.tableMenu(\''+idCount+'\', \''+i+'\', \'#table-' + boxesCount.toString() + '\')"':'')+' '+(i!==0?'ondblclick="form.editCollum(\''+idCount+'\', \''+i+'\', \'#table-' + boxesCount.toString() + '\')"':'')+' data-row="'+idCount+'" data-table="'+i+'" data-id="#table-' + boxesCount.toString() + '" class="tablecol row-'+idCount+'" style="background-color: ' + bg + '; width: '+(i===0?'40px':newWidth+'px')+';">'+(i===0?idCount:'')+'</td></tr>'
				}

				table = table + '</tbody></table></td>';
			}

			table = table + '</td></tr></table>';
			table = table + '<div class="hoverMenu"><div class="addTableRow" title="Add a row" onclick="form.addRow(\'#table-' + boxesCount.toString() + '\')"><span class="glyphicon glyphicon-plus"></span></div>';
			table = table + '<div class="removeTableRow" title="Remove a row" onclick="form.deleteRow(\'#table-' + boxesCount.toString() + '\')"><span class="glyphicon glyphicon-minus"></span></div>';
			table = table + '<br style="clear: left;" />';
			table = table + '</div>';
			table = table + '<div class="editAssetTable" onclick="form.editCustomTable(\'#table-' + boxesCount.toString() + '\')"><span class="glyphicon glyphicon-pencil"></span></div>';
			table = table + '<div class="hoverMenuRight"><div class="addTableCol" title="Add a row" onclick="form.addCol(\'#table-' + boxesCount.toString() + '\')"><span class="glyphicon glyphicon-plus"></span></div>';
			table = table + '<div class="removeTableCol" title="Remove a row" onclick="form.deleteCol(\'#table-' + boxesCount.toString() + '\')"><span class="glyphicon glyphicon-minus"></span></div>';
			table = table + '</div>';

			var addTable = $(table).css(css).appendTo("#formBuildGrid");

			$(".main > tbody > tr", "#table-" + boxesCount).sortable({ items: "> td:not(:first)" });
			$(".main > tbody > tr", "#table-" + boxesCount).disableSelection();

			$("#table-" + boxesCount)
			.draggable({ 
				grid: [ 20, 20 ], 
				containment: "#formBuildGrid", 
				scroll: false,
				drag: function(event,ui) { form.handleDrag(event, ui, $(this)); },
				stop: function(event,ui) { form.handleStop(event, ui, $(this)); },
			});

			if(top > 740) {
				var height_ = $("#formBuildGrid").height();
					height_ = height_ + $("#table-" + boxesCount).height();

				var css_ = {
					height: height_ + "px"
				};

				$("#formBuildGrid").css(css_);
			}


			boxesCount = boxesCount + 1;
			zIndex = zIndex + 1;
		} else {
			form.alertMessages("danger", "Error", "No or too much rows defined");
			return false;
		}
	} else {
		form.alertMessages("danger", "Error", "No or too much columns defined");
		return false;
	}

	return true;
}

$formBuilder.prototype.tableMenu = function(row, table, id) {
	var id_ = id.split("#");
		id_ = id_[1];

	if(document.getElementById(id_).addEventListener) {
		document.getElementById(id_).addEventListener('contextmenu', function(e) {
			e.preventDefault();

			// $(".table-" + table + " .row-" + row, id).on('contextmenu', function(e) {
				$("#contextMenu").show();
				$("#contextMenu").css({
					top: parseInt(e.clientY),
					left: parseInt(e.clientX)
				});

				var count = 0;
				$(".main > tbody > tr", id).children().each(function() {
					count++;
				});

				if(count > 2) {
					$("#deleteColumn").removeClass('disabled');
					$("#deleteColumn").attr("onclick", "form.deleteColumn('"+table+"', '"+id+"')");
				} else {
					$("#deleteColumn").addClass('disabled');
				}

				$("#deleteTable").click(function(e) {
					$(id).remove();
				});
			// });
		}, false);
	}
}

$formBuilder.prototype.headMenu = function(table, id) {
	var id_ = id.split("#");
		id_ = id_[1];
	if(document.getElementById(id_).addEventListener) {
		document.getElementById(id_).addEventListener('contextmenu', function(e) {
			e.preventDefault();
			$(".table-" + table + " .tablehead", id).on('contextmenu', function(e) {
				e.preventDefault;
				$("#contextMenuHead").show();
				$("#contextMenuHead").css({
					top: parseInt(e.clientY),
					left: parseInt(e.clientX)
				});

				var count = 0;
				$(".main > tbody > tr", id).children().each(function() {
					count++;
				});

				if(count > 2) {
					$("#deleteColumnH").removeClass('disabled');
					$("#deleteColumnH").attr("onclick", "form.deleteColumn('"+table+"', '"+id+"')");
				} else {
					$("#deleteColumnH").addClass('disabled');
				}

				$("#deleteTableH").click(function(e) {
					$(id).remove();
				});
			});
		}, false);
	}
}

$formBuilder.prototype.deleteColumn = function(table, id) {

	var count = 0;
	$(".main > tbody > tr", id).children().each(function() {
		count++;
	});

	if(count > 2) {
		$(".main > tbody > tr > td > .table-" + table, id).parent().detach();

		$(".main > tbody > tr > td > .table-" + table, id).detach();
		var number = 1;
							
		$(".main > tbody > tr > td > .nonId", id).each(function() {
			$(this).attr("class", "child nonId table-"+number);
			$(this).children().each(function() {
				$(this).children().each(function() {
					$(this).children().each(function() {
						if($(this).attr("class") != "tablehead") {
							$(this).attr("ondblclick", "");

							var id_ = $(this).data("id");
							var row = $(this).data("row");

							$(this).attr("data-table", number);

							$(this).attr("oncontextmenu", "");
							$(this).attr("oncontextmenu", "form.tableMenu('"+row+"', '"+number+"', '"+id_+"')")

							$(this).attr("ondblclick", "form.editCollum('"+row+"', '"+number+"', '"+id_+"')");
						} else {
							$(this).attr("ondblclick", "");
							$(this).attr("oncontextmenu", "");

							$(this).attr("oncontextmenu", "form.headMenu('"+number+"', '"+id+"')")
							$(this).attr("ondblclick", "form.editHead('"+number+"', '"+id+"')");
						}
					});
				});
			});
			number++;
		});

			
		count = count - 2;

		var width = Math.floor((form.config.tableConfig.tableWidth/count));

		$(".nonId > tbody > tr", id).children().each(function() {
			$(this).css({width: width+'px'});
		});
	}
}

/**
 * load the table information
 * 
 * @param  string	 id
 * @return boolean
 */
$formBuilder.prototype.editCustomTable = function(id) {
	if(typeof(id) != "undefined") {

		var title = $(".tableName", id).html();
		
		var scope = angular.element($("#formWrapper")).scope();
		scope.safeApply(function() {
			scope.tableName = title;
			scope.tableId = id;
			scope.windowTpl = 'edittable';
			scope.showWindow = true;
		});

		return true;
	}

	return false;
}

/**
 * load the table information
 * 
 * @param  string	 id
 * @param  string    title
 * @return boolean
 */
$formBuilder.prototype.saveCustomTable = function(id, title) {
	if(typeof(title) != "undefined" && typeof(id) != "undefined") {

		$(".tableName", id).html(title);

		return true;
	}

	return false;
}

/**
 * edit the collum of the table
 * 
 * @param  int 		col
 * @param  int 		table
 * @return boolean
 */
$formBuilder.prototype.editCollum = function(col, table, id) {
	if(typeof(col) == "undefined") {
		return false;
	} else if(typeof(table) == "undefined") {
		return false;
	} else {
		table = parseInt(table),
		col = parseInt(col);

		var content = $(".table-" + table + " .row-" + col, id).html();
			content = content.replace(/<br>/g, "\r\n");
		var scope = angular.element($("#formWrapper")).scope();

		var currentLocation = window.location;
			currentLocation.hash;
		if(currentLocation.hash != "#/preview/") {
			scope.$apply(function() {
				scope.windowTpl = 'whitebox';
				scope.showWindow = true;
				scope.contentId = ".row-" + col + ";.table-" + table + ";"+id;
				scope.content = content;
			});
		}
	}
}

/**
 * edit the head of the table
 * 
 * @param  int 		col
 * @param  int 		table
 * @return boolean
 */
$formBuilder.prototype.editHead = function(table, id) {
	if(typeof(table) == "undefined") {
		return false;
	} else {
		table = parseInt(table);

		var content = $(".table-" + table + " .tablehead", id).html();

		var scope = angular.element($("#formWrapper")).scope();

		var currentLocation = window.location;
			currentLocation.hash;
		if(currentLocation.hash != "#/preview/") {
			scope.$apply(function() {
				scope.windowTpl = 'whitebox';
				scope.showWindow = true;
				scope.contentId = ".tablehead;.table-" + table + ";"+id;
				scope.content = content;
			});
		}
	}
}

/**
 * adds a collum to the table
 *
 * @final
 * @param string e_
 * @return boolean
 */
$formBuilder.prototype.addCol = function(e_) {
	if(e_.length === 0) return false;

	var t_ = e_;
	var c = 0; // max amount of rows
	var c_ = 0; // amount of cols

	$(".child > tbody > tr", t_).children().each(function() {
		c++;
	});
	$(".child > thead > tr", t_).children().each(function() {
		c_++;
	});

	c_ = c_;
	c = c/c_;

	if(c_ <= (form.config.tableConfig.custom.maxCols - 1)) {
		var w_ = Math.floor((form.config.tableConfig.tableWidth/(c_)));

		$(".nonId > tbody > tr", t_).children().each(function() {
			$(this).css({width: w_+'px'});
		});

		var a_ = '<td><table class="child nonId table-'+c_+'">';
			a_ = a_ + '<thead>';
			a_ = a_ + '<tr>';
			a_ = a_ + '<td oncontextmenu="form.headMenu(\''+c_+'\', \'' + t_ + '\')" ondblclick="form.editHead(\''+c_+'\', \''+t_+'\')" class="tablehead" style="width: '+w_+'px;"></td>';
			a_ = a_ + '</tr></thead>';
			a_ = a_ + '<tbody>';

		for(var d = 0; d < c; d++) { 
			var bg = (d%2?'#fafafa':'#efefef');
			var i_ = d + 1;
			a_ = a_ + '<tr>';
			a_ = a_ + '<td oncontextmenu="form.tableMenu(\''+i_+'\', \''+c_+'\', \'' + t_ + '\')" ondblclick="form.editCollum(\''+i_+'\', \''+c_+'\', \''+t_+'\')" class="tablecol row-'+i_+'" style="background-color: ' + bg + '; width: '+w_+'px;" data-row="'+i_+'" data-table="'+c_+'" data-id="'+t_+'"></td>'
			a_ = a_ + '</tr>';
		}
			a_ = a_ + '</tbody>';
			a_ = a_ + '</table></td>';

		$(".main > tbody > tr", t_).append(a_);
	}

	return true;
}

/**
 * deletes a collum from the table
 *
 * @final
 * @param  string item
 * @return boolean
 */
$formBuilder.prototype.deleteCol = function(item) {
	if(item.length === 0) return false;

	var table = item;
	var count = 0; // max amount of rows
	var count_ = 0; // amount of cols

	$(".child > thead > tr", table).children().each(function() {
		count_++;
	});

		count_ = count_ - 1;
	var check_ = count_ - 1;

	if(check_ >= 1) {
		var width = Math.floor((form.config.tableConfig.tableWidth/(check_)));

		$(".nonId > tbody > tr", table).children().each(function() {
			$(this).css({width: width+'px'});
		});

		$(".table-"+count_, table).parent().remove();
		$(".table-"+count_, table).remove();
	}

	return true;
}

/**
 * adds a row to a table
 *
 * @final
 * @param string item 
 * @return boolean
 */
$formBuilder.prototype.addRow = function(item) {
	if(item.length === 0) return false;


	var table = item;
	var count = 0; // max amount of rows
	var count_ = 0; // amount of cols

	var cssBg = ""; // last background color

	$(".child > tbody > tr", table).children().each(function() {
		var self = $(this);

		cssBg = self.css("backgroundColor");
		count++;
	});
	$(".child > thead > tr", table).children().each(function() {
		var self = $(this);

		count_++;
	});

	var bg = (cssBg==='rgb(239, 239, 239)'?'#fafafa':'#efefef');
	var _count = (count/count_) + 1; // amount of rows
	if(_count < (form.config.tableConfig.custom.maxRows + 1)) {
		var row = '<tr><td class="tablecol row-'+_count+'" style="background-color: '+bg+'; width: 40px">' + _count.toString() + '</td></tr>';

		$(".idTable > tbody", table).append(row);

		count_ = count_-1;

		var width = Math.floor((form.config.tableConfig.tableWidth/count_));

		var row = '<tr><td ondblclick="form.editCollum(\''+_count+'\', \''+1+'\')" class="tablecol row-'+_count+'" style="background-color: '+bg+'; width: '+width+'px"></td></tr>';
		$(".nonId > tbody", table).append(row);

		return true;
	} else {
		return false;
	}
}

/**
 * deletes a row from a table
 *
 * @final
 * @param string item  
 * @return boolean
 */
$formBuilder.prototype.deleteRow = function(item) {
	if(item.length === 0) return false;

	var table = item;
	var count = 0; // max amount of rows
	var count_ = 0; // amount of cols


	$(".child > tbody > tr", table).children().each(function() {
		count++;
	});
	$(".child > thead > tr", table).children().each(function() {
		count_++;
	});

	var _count = count/count_; // amount of rows
	if(_count > 1) {
		$(".child > tbody > tr .row-"+_count, table).remove();
	}
	
	return true;
}

/**
 * creates an input field
 * 
 * @param  string input
 * @param  string label
 * @param  string value
 * @return boolean
 **/
$formBuilder.prototype.createInputField = function(input, label, value) {
	if(typeof(input) === "undefined") {
		form.alertMessages("danger", "Error", "No input selected");
		return false;
	}
	if(typeof(label) === "undefined") {
		form.alertMessages("danger", "Error", "No label defined");
		return false;
	}
	if(typeof(value) === "undefined") {
		form.alertMessages("danger", "Error", "No value defined");
		return false;
	}

	switch(input) {
		case "text":
			var input 	  = "simple";
			var inputType = "text";
		break;
		case "check":
			var input 	  = "simple";
			var inputType = "checkbox";
		break;
		case "radio":
			var input 	  = "simple";
			var inputType = "radio";
		break;
		case "drop":
			var input 	  = "complex";
			// var inputType = "text";
		break;
		default:
			throw("Unknown input type");
		break;
	}

	if(input == "simple") {
		var create = '<div id="input-' + boxesCount.toString() + '" data-type="input" class="input" data-type="input">';
		create = create + '<div class="inputHead">Input field</div>';
		create = create + '<div class="inputContent">';
		create = create + '<label><input type="' + inputType + '" class="form-ui-input" name="'+value+'" /> ' + label + '</label>';
		create = create + '</div>';
		create = create + '</div>';

		var top = 0;
		$('#formBuildGrid').children().each(function() {
			var self = $(this);
				top = Math.max(top, self.position().top);
		});
			top = top + 60;
		var left = 20;

		if(top > 740) {
			var height_ = $("#formBuildGrid").height();
				height_ = height_ + 60;

			var css_ = {
				height: height_ + "px"
			};

			$("#formBuildGrid").css(css_);
		}

		var css = {
			top: top,
			left: left,
			minWidth: 40,
			maxWidth: 400,
			minHeight: 100,
			cursor: 'move',
			position: 'absolute',
			zIndex: zIndex,
		};

		var add = $(create).css(css).appendTo($("#formBuildGrid"));		
		$('#input-' + boxesCount.toString())
		.draggable({ 
			grid: [ 20, 20 ], 
			containment: "#formBuildGrid", 
			scroll: false,
			drag: function(event,ui) { form.handleDrag(event, ui, $(this)); },
			stop: function(event,ui) { form.handleStop(event, ui, $(this)); },
		});

		boxesCount = boxesCount + 1;
		zIndex = zIndex + 1;

		return true;
	} else {
		var scope = angular.element($("#formWrapper")).scope();
		scope.safeApply(function() {
			scope.windowTpl = 'dropdown';
			scope.dropdown = [];
			scope.dropdownLabel = label;
			scope.showWindow = true;
		});

		return false;
	}
}

/**
 * creates a dropdown menu as input field
 * 
 * @param  object 	drop
 * @param  string	label
 * @return boolean
 */
$formBuilder.prototype.createDropdown = function(drop, label) {
	if(typeof(drop) != "undefined") {
		var create = '<div id="input-' + boxesCount.toString() + '" data-type="input" class="input">';
		create = create + '<div class="inputHead">Input field</div>';
		create = create + '<div class="inputContent">';
		create = create + '<div class="selected" style="float: left; margin-right: 10px;"><select name="input-' + boxesCount.toString() + '">';
		for(var i in drop) {
			var d_ = JSON.stringify(drop[i]);
				d_ = d_.split("\":\"");
				d_ = d_[1];
				d_ = d_.split("\"}");
				d_ = d_[0];
				d_ = d_.split("\"");
				d_ = d_[0];
				create = create + "<option value=\""+d_+"\">"+d_+"</option>";
		}
		create = create + "</select></div> <label> " + label + "</label><br style=\"clear: left;\">";
		create = create + '</div>';
		create = create + '<div class="editAssetTable" onclick="form.editDropDown(\'#input-' + boxesCount.toString() + '\')"><span class="glyphicon glyphicon-pencil"></span></div>';
		create = create + '</div>';

		var top = 0;
		$('#formBuildGrid').children().each(function() {
			var self = $(this);
				top = Math.max(top, self.position().top);
		});
			top = top + 60;
		var left = 20;

		if(top > 740) {
			var height_ = $("#formBuildGrid").height();
				height_ = height_ + 60;

			var css_ = {
				height: height_ + "px"
			};

			$("#formBuildGrid").css(css_);
		}

		var css = {
			top: top,
			left: left,
			minWidth: 40,
			maxWidth: 400,
			minHeight: 100,
			cursor: 'move',
			position: 'absolute',
			zIndex: zIndex,
		};

		var add = $(create).css(css).appendTo($("#formBuildGrid"));		
		$('#input-' + boxesCount.toString())
		.draggable({ 
			grid: [ 20, 20 ], 
			containment: "#formBuildGrid", 
			scroll: false,
			drag: function(event,ui) { form.handleDrag(event, ui, $(this)); },
			stop: function(event,ui) { form.handleStop(event, ui, $(this)); },
		});

		boxesCount = boxesCount + 1;
		zIndex = zIndex + 1;

		return true;
	} else {
		return false;
	}
}

/**
 * collects to options of the select box
 * 
 * @param  int		id
 * @return boolean
 */
$formBuilder.prototype.editDropDown = function(id) {
	var count = 0;
	var push = [];
	$("select > option", id).each(function() {
		var html = $(this).context.innerHTML;
		var obj = {};
		obj["drop_"+count] = html;
		push.push(obj);
		count++;
	});

	var scope = angular.element($("#formWrapper")).scope();
	scope.safeApply(function() {
		scope.dropdown = push;
		scope.dropdownId = id;
		scope.windowTpl = 'dropdown';
		scope.showWindow = true;
	});

	return true;
}

/**
 * updates the dropdown
 * 
 * @param  object	 drops
 * @param  string	 id
 * @return boolean
 */
$formBuilder.prototype.updateDropDown = function(drops, id) {
	if(typeof(drops) != "undefined" && typeof(id) != "undefined") {
		$("select", id).remove();
		var create = '<select name="input-' + boxesCount.toString() + '">';
		for(var i in drops) {
			var d_ = JSON.stringify(drops[i]);
				d_ = d_.split("\":\"");
				d_ = d_[1];
				d_ = d_.split("\"}");
				d_ = d_[0];
				d_ = d_.split("\"");
				d_ = d_[0];
				create = create + "<option value=\""+d_+"\">"+d_+"</option>";
		}
		create = create + "</select>";

		$(".selected", id).append(create);

		return true;
	}

	return false;
}

/**
 * handles the drag function

 * @param  obj event
 * @param  obj ui
 * @return boolean
 */
$formBuilder.prototype.handleDrag = function(event, ui, item) {
	$("#trashBin").show(0);

	var elHeight = item.outerHeight();

	var bottom = ui.position.top + elHeight;
	var	gridNeu = gridHeight - 20;


	if(bottom >= gridNeu) {
		$("#formBuildGrid").css({height:gridHeight+60});
		gridHeight = gridHeight+60;
	}

	var elements = {}, link_cache = {}, offset_cache = {};

	$('#formBuildGrid').children().each(function() {
		var self = $(this), id = self.attr('id'), position = self.position(), width = self.outerWidth(), height = self.outerHeight();
		if(id != "trashBin") {
			elements[id] = {
				id: id,
				position: position,
				offsets:position,
				width: width,
				height: height,
				x: [position.left, position.left + width],
				y: [position.top, position.top + height]
			};

			link_cache[position.top] = link_cache[position.top] || [];
			link_cache[position.top].push(id);
		}
	});

	var collision = false;

	$.each(elements, function() {
		var self = $(this)[0], top = null, left = null;

		$.each(elements, function() {
			var _self = $(this)[0];

			if(_self == self) {
				return;
			}
			
			var _x = (self.x[0] < _self.x[1] && self.x[1] > _self.x[0]),
				_y = (self.y[0] < _self.y[1] && self.y[0] >= _self.y[0]) || (_self.y[0] < self.y[1] && _self.y[0] >= self.y[0]);
							
			if(
				(
					self.x[0] >= _self.x[0] && self.x[0] <= _self.x[1]
				||
					self.x[1] >= _self.x[0] && self.x[1] <= _self.x[1]
				)
				&&
				(
					self.y[0] >= _self.y[0] && self.y[0] <= _self.y[1]
				||
					self.y[1] >= _self.y[0] && self.y[1] <= _self.y[1]
				)
			) {
				if(form.config.debug === true) {
					var clientH = ui.offset.top + ui.helper.context.clientHeight;
					var clientW = ui.offset.left + ui.helper.context.clientWidth;
					var debugContent = "Client Width: " + clientW + " (" + ui.helper.context.id + ") <br />Client Height: " + clientH + " (" + ui.helper.context.id + ")<br />Obj. Height: " + self.height + " (" + self.id + ")<br />Obj. Width: " + self.width + " (" + self.id + ")<br />Y-POS: " + ui.offset.top + " (" + ui.helper.context.id + ")<br />X-POS: " + ui.offset.left + " (" + ui.helper.context.id + ")<br />Obj. Y: " + self.y + " (" + self.id + ")<br />Obj. X: " + self.x + " (" + self.id + ")<br /><br /><pre>" + JSON.stringify(self, null, 2) + "</pre><br /><br />Collision";
					$(".debug").html(debugContent);
				}

				collision = true;
			} else {
				var clientH = ui.offset.top + ui.helper.context.clientHeight;
				var clientW = ui.offset.left + ui.helper.context.clientWidth;
				var debugContent = "Client Width: " + clientW + " (" + ui.helper.context.id + ") <br />Client Height: " + clientH + " (" + ui.helper.context.id + ")<br />Obj. Height: " + self.height + " (" + self.id + ")<br />Obj. Width: " + self.width + " (" + self.id + ")<br />Y-POS: " + ui.offset.top + " (" + ui.helper.context.id + ")<br />X-POS: " + ui.offset.left + " (" + ui.helper.context.id + ")<br />Obj. Y: " + self.y + " (" + self.id + ")<br />Obj. X: " + self.x + " (" + self.id + ")<br /><br /><pre>" + JSON.stringify(self, null, 2) + "</pre>";
				$(".debug").html(debugContent);
			}
		});
	});

	if(collision === true) {
		return false;
	}
}

/**
 * This function handles the stop return of draggable
 * It calculates the current position and height and pushes it into an array
 *
 * @param event
 * @param ui
 */
$formBuilder.prototype.handleStopResize = function(event, ui) {
	var id = ui.element.context.id;
  	var width = ui.size.width;
  	var height = ui.size.height;

  	var new_width = Math.round(width / 20) * 20;
  	var new_height = Math.round(height / 20) * 20;

  	$("#"+id).css({
  		width: new_width + "px",
  		height: new_height + "px"
  	});

  	var position = {name: id, dim: [{height: height}, {width: width}],position: [{x: 0}, {y: 0}]};

  	if(boxes.box.length > 0) {
	 	for(var i in boxes.box) {
	 		if(boxes.box[i].name == id) {
	  			var match = true;
	  			var set = i;
	  			break;
	  		} else {
	  			var match = false;
	  		}
	  	}
	} else {
		var match = false;
	}

 	if(match === false) {
  		boxes.box.push(position);
  	} else {
  		if(match === true) {
  			boxes.box[set].dim[0].height = height;
  			boxes.box[set].dim[1].width = width;
		}
  	}
}

/**
 * handles the stop function
 * 
 * @param  obj event
 * @param  obj ui
 */
$formBuilder.prototype.handleStop = function(event, ui, item) {
	$("#trashBin").hide(0);	

	var id = item.attr("id");

  	var offsetXPos = parseInt(ui.offset.left);
  	var offsetYPos = parseInt(ui.offset.top);

  	var width = item.css('width');
  	var height = item.css('height');

  	var position = {name: id, dim: [{height: height}, {width: width}],position: [{x: offsetXPos}, {y: offsetYPos}]};

  	// if(form.collision === true) {
  		// $("#"+id).css({
  			// left: 0,
  			// top: 0
  		// });
  	// }

  	if(boxes.box.length > 0) {
	 	for(var i in boxes.box) {
	 		if(boxes.box[i].name == id) {
	  			var match = true;
	  			var set = i;
	  			break;
	  		} else {
	  			var match = false;
	  		}
	  	}
	} else {
		var match = false;
	}

 	if(match === false) {
  		boxes.box.push(position);
  	} else {
  		if(match === true) {
  			boxes.box[set].dim[0].height = height;
  			boxes.box[set].dim[1].width = width;

  			boxes.box[set].position[0].x = offsetXPos; // to save it in the db	| can be later displayed at any position 			| x - 500
 			boxes.box[set].position[1].y = offsetYPos; // the same				| just calculate the position of the gridbox then 	| y + 20
  		}
  	}
}

/**
 * deletes current box
 *
 * @final
 * @param  string box
 * @return bool
 */
$formBuilder.prototype.deleteBox = function(event, ui, id) {
	if(typeof(id) == "undefined" && event != "" && ui != "") {
		if(ui.draggable.context.id.length > 0) {
			var idName = ui.draggable.context.id;
		} else {
			var idName = ui.draggable.context.firstChild.id;
		}
	} else {
		var idName = id;
			idName = id.split("#");
			idName = id[0];
	}

	if($("#" + idName).attr("class")) {
		var classes  = $("#" + idName).attr("class");
		var classesN = classes.split(" ");
		if(classesN[0] == "whitebox") {
			var scope = angular.element($("#formWrapper")).scope();
			scope.$apply(function() {
				scope.showWindow = false;
				scope.windowTpl = 'img';
			});
		}
	} 

	var type = $("#" + idName).data("type");

	if(type == "assetTable") {
		var scope = angular.element($("#formWrapper")).scope();
		scope.safeApply(function() {
			scope.alreadyCreated = false;
		});
	}

	$("#" + idName).children().each(function() {
		$(this).remove();
	});
	$("#" + idName).remove();

	if(containers.length > 0) {
		for(var i in containers) {
			if(idName == containers[i].id) {
				var set = i;
				var match = true;
				break;
			} else {
				var match = false;
			}
		}
		if(match === true) { 
			containers.splice(i, 1);
		}
	}

	if(form.config.debug === true) console.log("#" + idName + " delete");
}

/**
 * form.class.renderForm
 *
 * render engine for the boxes
 * 
 * @param  object 	data
 * @return boolean  
 */
$formBuilder.prototype.renderForm = function(data) {
	if(data.length === 0) return false;

	$("#formBuildGrid").html("");
	
	// add the trashbin to the grid
	$("#formBuildGrid").append('<div id="trashBin"><div id="trashContent"><span class="glyphicon glyphicon-trash" style="font-size: 48px;"></span></div></div>');

	$("#formBuildGrid").droppable({
		drop: function(event, ui) { form.dropFunction(event, ui); }
	});

	// make it droppable
	$("#trashBin").droppable({
		tolerance: 'pointer',
		drop: function(event, ui) { form.deleteBox(event, ui); }
	});

	// get the form informations
	for(var c in data.rows.info) {
		var form_id = data.rows.info[c].form_id;
		var name = data.rows.info[c].form_name;
		var desc = data.rows.info[c].form_description;
		var grid_height = data.rows.info[c].grid_height;
		var grid_width = data.rows.info[c].grid_width;
	}

	// generate the boxes
	for(var i in data.rows.rows) {
		var top =  parseInt(data.rows.rows[i].pos_y);
		var left = parseInt(data.rows.rows[i].pos_x);

		switch(data.rows.rows[i].type) {
			case "whitebox":
				if(!form.createWhiteBox(0,0,true, left, top, data.rows.rows[i].width, data.rows.rows[i].height, data.rows.rows[i].div_id, data.rows.rows[i].content, data.rows.rows[i].id)) {
					form.alertMessages("danger", "Error", "Can't create white box");
				}
			break;
			case "image":
				var img_con = data.rows.rows[i].content;
					img_con = img_con.split("id=\"");
				var	img_id 	= img_con[1];
					img_id  = img_id.split("\"");
				var	img_id_ = img_id[0];

				if(!form.createImageBox(img_id[4], 0, 0, true, data.rows.rows[i].div_id, img_id_, left, top, data.rows.rows[i].width, data.rows.rows[i].height, data.rows.rows[i].id)) {
					form.alertMessages("danger", "Error", "Can't create image box");
				}

			break;
			case "customTable":
				var css = {
					width: data.rows.rows[i].width + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute'
				};

				var gen = '<div id="' + data.rows.rows[i].div_id + '" data-information="'+data.rows.rows[i].id+'" data-type="'+data.rows.rows[i].type+'" class="' + data.rows.rows[i].classes +'">';
					gen = gen + data.rows.rows[i].content
					gen = gen + '<div class="hoverMenu"><div class="addTableRow" title="Add a row" onclick="form.addRow(\'#' + data.rows.rows[i].div_id + '\')"><span class="glyphicon glyphicon-plus"></span></div>';
					gen = gen + '<div class="removeTableRow" title="Remove a row" onclick="form.deleteRow(\'#' + data.rows.rows[i].div_id + '\')"><span class="glyphicon glyphicon-minus"></span></div>';
					gen = gen + '<br style="clear: left;" />';
					gen = gen + '</div>';
					gen = gen + '<div class="editAssetTable" onclick="form.editCustomTable(\'#'+data.rows.rows[i].div_id+'\')"><span class="glyphicon glyphicon-pencil"></span></div>';
					gen = gen + '<div class="hoverMenuRight"><div class="addTableCol" title="Add a row" onclick="form.addCol(\'#' + data.rows.rows[i].div_id + '\')"><span class="glyphicon glyphicon-plus"></span></div>';
					gen = gen + '<div class="removeTableCol" title="Remove a row" onclick="form.deleteCol(\'#' + data.rows.rows[i].div_id + '\')"><span class="glyphicon glyphicon-minus"></span></div>';
			
					gen = gen + '</div>';

				var add = $(gen).css(css).appendTo($("#formBuildGrid"));

				var self = '#' + data.rows.rows[i].div_id;

				$(".main > tbody > tr", self).sortable({ items: "> td:not(:first)" });
				$(".main > tbody > tr", self).disableSelection();

				$(self).draggable({ 
					grid: [ 20, 20 ], 
					containment: "#formBuildGrid", 
					scroll: false,
					drag: function(event,ui) { form.handleDrag(event, ui, $(this)); },
					stop: function(event,ui) { form.handleStop(event, ui, $(this)); },
				});
			break;
			case "assetTable":
				var css = {
					width: data.rows.rows[i].width + 'px',
					height: data.rows.rows[i].height + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute'
				};

				var gen = '<div id="' + data.rows.rows[i].div_id + '" data-information="'+data.rows.rows[i].id+'" data-type="'+data.rows.rows[i].type+'" class="' + data.rows.rows[i].classes +'">';
					gen = gen + data.rows.rows[i].content;
					gen = gen + '</div>';

				var add = $(gen).css(css).appendTo($("#formBuildGrid"));

				var self = '#' + data.rows.rows[i].div_id;

				$("table > thead > tr", self).sortable({ items: "> td:not(:first)" });
				$("table > thead > tr > .noId", self).resizable({
					handles: 'e, w',
					autoHide: true
				});
				$("table > thead > tr", self).disableSelection();

				$(self).draggable({ 
					grid: [ 20, 20 ], 
					containment: "#formBuildGrid", 
					scroll: false,
					drag: function(event,ui) { form.handleDrag(event, ui, $(this)); },
					stop: function(event,ui) { form.handleStop(event, ui, $(this)); },
				});
				var scope = angular.element($("#formWrapper")).scope();
				scope.safeApply(function() {
					scope.alreadyCreated = true;
				});
			break;
			case "input":
				var css = {
					width: data.rows.rows[i].width + 'px',
					height: data.rows.rows[i].height + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute'
				};

				var gen = '<div id="' + data.rows.rows[i].div_id + '" data-information="'+data.rows.rows[i].id+'" data-type="'+data.rows.rows[i].type+'" class="' + data.rows.rows[i].classes +'">';
					gen = gen + data.rows.rows[i].content;
					gen = gen + '</div>';

				var add = $(gen).css(css).appendTo($("#formBuildGrid"));

				var self = '#' + data.rows.rows[i].div_id;

				$(self).draggable({ 
					grid: [ 20, 20 ], 
					containment: "#formBuildGrid", 
					scroll: false,
					drag: function(event,ui) { form.handleDrag(event, ui, $(this)); },
					stop: function(event,ui) { form.handleStop(event, ui, $(this)); },
				});
			break;
			case "variable":
				var var_con = data.rows.rows[i].content;

				if(!form.createNewVariable(var_con, top, left)) {
					form.alertMessages("danger", "Error", "Can't create variable box");
				}
			break;
			default:
				return false;
			break;
		}
	}

	var scope = angular.element($("#formWrapper")).scope();
	scope.safeApply(function() {
		scope.formInformation["response"]["data"] = form_id;
		scope.data["name"] = name;
		scope.data["desc"] = desc;
	});

	form.alertMessages('success', 'Success', 'Loaded successful the form');

	return true;
}

/** 
 *	displays the preview
 *
 *  @param  object data
 *	@return boolean
 */
$formBuilder.prototype.renderPreviewForm = function(data, mode, action) {
	if(data.length === 0) return false;

	$("#formBuildGridPreview").html("");

	for(var c in data.rows.info) {
		var form_id = data.rows.info[c].form_id;
		var name = data.rows.info[c].form_name;
		var desc = data.rows.info[c].form_description;
		var grid_height = data.rows.info[c].grid_height;
		var grid_width = data.rows.info[c].grid_width;
	}

	var scope = angular.element($("#formWrapper")).scope();
	scope.safeApply(function() {
			scope.formInformation["response"]["data"] = form_id;
			scope.data["name"] = name;
			scope.data["desc"] = desc;
	});

	var completeHeight = 0;

	for(var i in data.rows.rows) {
		var top =  parseInt(data.rows.rows[i].pos_y);
		var left = parseInt(data.rows.rows[i].pos_x);

		completeHeight += (top - parseInt(data.rows.rows[i].height)) + parseInt(data.rows.rows[i].height);

		switch(data.rows.rows[i].type) {
			case "whitebox":
				var css = {
					width: data.rows.rows[i].width + 'px',
					height: data.rows.rows[i].height + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute'
				};

				var whiteBox = $('<div id="' + data.rows.rows[i].div_id + '" class="' +  data.rows.rows[i].classes + '"></div>').css(css);
				var whiteBoxContent = $('<div class="whiteboxContent" >' + data.rows.rows[i].content + '</div>');

				whiteBox.append(whiteBoxContent);
				whiteBox.appendTo($('#formBuildGridPreview'));

				if(whiteBoxContent.outerHeight() > whiteBox.height()) {
					whiteBox.height(whiteBoxContent.outerHeight());
				}

			break;
			case "image":
				var css = {
					width: data.rows.rows[i].width + 'px',
					height: data.rows.rows[i].height + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute',
					cursor: 'default'
				};
				var css_ = {
					cursor: 'default'
				}

				var content = data.rows.rows[i].content;
					content = content.split("<div");
					content = content[0];

				var gen = $('<div id="' + data.rows.rows[i].div_id + '"></div>').css(css);
				var	image = $(content).css(css_);
				
				gen.append(image);

				var add = $(gen).appendTo($("#formBuildGridPreview"));
			break;
			case "assetTable":
				var css = {
					width: data.rows.rows[i].width + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute'
				};

				var gen = '<div id="' + data.rows.rows[i].div_id + '" class="' +  data.rows.rows[i].classes + '">';
					gen = gen + data.rows.rows[i].content;
					gen = gen + '</div>';

				var add = $(gen).css(css).appendTo($("#formBuildGridPreview"));

				if(typeof(data.rows.table) != "undefined") {
					var t_id = 1;
					var p 	 = data.rows.table;
					var c_ 	 = 1;
					console.log(p);

					for(var b in p) {
						var r = 0;
						for(var k in p[b]) {
							if(!p[b][k].input && !p[b][k].checkbox) {
								var bg = (r%2?'#fafafa':'#efefef');
								var d_ = JSON.stringify(p[b][k]);
									d_ = d_.split("\":\"");
									d_ = d_[1];
									d_ = d_.split("\"}");
									d_ = d_[0];
								if(d_ == "") d_ = "n/a";
									d_ = d_.substring(0, 10);
								var t_ = '<tr><td style="text-align: center; background-color: '+bg+'; height: 35px;">'+d_+'</td></tr>';
								var _i = "#"+data.rows.rows[i].div_id;
								$(".tab-"+c_+" > tbody", _i).append(t_);
							} else {
								if(p[b][k].input) {
									var bg = (r%2?'#fafafa':'#efefef');
									var t_ = '<tr><td style="text-align: center; background-color: '+bg+'; height: 35px;">'+p[b][k].input+'</td></tr>';
									var _i = "#"+data.rows.rows[i].div_id;
									$(".tab-"+c_+" > tbody", _i).append(t_);
								} else {
									if(p[b][k].checkbox) {
										var bg = (r%2?'#fafafa':'#efefef');
										var t_ = '<tr><td style="text-align: center; background-color: '+bg+'; height: 35px;">'+p[b][k].checkbox+'</td></tr>';
										var _i = "#"+data.rows.rows[i].div_id;
										$(".tab-"+c_+" > tbody", _i).append(t_);
									}
								}
							}
							r++;
						}
						c_++;
					}
					var q = 1;
					for(var w = 0; w<r; w++) {
						var bg = (w%2?'#fafafa':'#efefef');
						var _i = "#"+data.rows.rows[i].div_id;
						$(".idTable > tbody", _i).append("<tr><td style=\"text-align: center; background-color: "+bg+"; height: 35px;\">"+q+"</td></tr>");
						q++;
					}
				}
			break;
			case "variable":
				var css = {
					height: data.rows.rows[i].height + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute'
				};

				var gen = '<div id="' + data.rows.rows[i].div_id + '" class="' +  data.rows.rows[i].classes + '">';
					gen = gen + data.rows.rows[i].content;
					gen = gen + '</div>';

				var add = $(gen).css(css).appendTo($("#formBuildGridPreview"));
			break;
			case "customTable":
				var css = {
					width: data.rows.rows[i].width + 'px',
					height: data.rows.rows[i].height + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute'
				};

				var content = data.rows.rows[i].content;
					content = content.split("style=\"cursor: move;\"");
					content = content[0]+content[1];

				var gen = '<div id="' + data.rows.rows[i].div_id + '" class="table">';
					gen = gen + content;
					gen = gen + '</div>';

				var add = $(gen).css(css).appendTo($("#formBuildGridPreview"));
			break;
			case "input":
				var css = {
					width: data.rows.rows[i].width + 'px',
					height: data.rows.rows[i].height + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute'
				};

				var gen = '<div id="' + data.rows.rows[i].div_id + '" class="input">';
					gen = gen + data.rows.rows[i].content;
					gen = gen + '</div>';

				var add = $(gen).css(css).appendTo($("#formBuildGridPreview"));
			break;
			default:
				var css = {
					width: data.rows.rows[i].width + 'px',
					height: data.rows.rows[i].height + 'px',
					top: top + 'px',
					left: left + 'px',
					position: 'absolute'
				};

				var gen = '<div id="' + data.rows.rows[i].div_id + '">';
					gen = gen + data.rows.rows[i].content;
					gen = gen + '</div>';

				var add = $(gen).css(css).appendTo($("#formBuildGridPreview"));
			break;
		}
	}

	if(completeHeight > grid_height) {
		$("#formBuildGridPreview").css({height: completeHeight+"px"});
	}

	if(typeof(mode) != "undefined" && mode === true) {
		if(typeof(action) != "undefined" && action == "job") {
			form.alertMessages('success', 'Success', 'Created a preview of the job');	
		} else {
			form.alertMessages('success', 'Success', 'Created a preview of the form');
		}
	}
	return true;
}

/**
 * initialize a new form
 * 
 * @return boolean
 */
$formBuilder.prototype.createNewForm = function() {
	var scope = angular.element($("#formWrapper")).scope();
	scope.changeFormId();
	scope.data["name"] = "";
	scope.data["desc"] = "";
	scope.alreadyCreated = false;

	$("#formBuildGrid").css({height: '800px'});

	return true;
}

/**
 * creates the beautiful alert messages (:
 *
 * @final
 * @param  string type
 * @param  string title
 * @param  string message
 * @return return boolean
 */
$formBuilder.prototype.alertMessages = function(type, title, message) {
	if(type.length === 0) return false;
	if(title.length === 0) return false;
	if(message.length === 0) return false;
	$("#waContent").show(0).append('<div class="alert alert-'+type+' alert-'+alert_.toString()+'" role="alert" style="z-index: 9990;"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4>'+title+':</h4> <p>'+message+'</p></div>');
	$(".alert-"+alert_.toString()).delay(2500).hide(0);
	$(".alert-"+alert_.toString()).alert()
	alert_ = alert_ + 1;

	return true;
}

/**
 * turn off able console.log
 *
 * @final
 * @param  string message
 * @return console.log
 */
$formBuilder.prototype.devLog = function(message) {
	if(form.config.debug === true) {
		console.log(message);
	}
}

/**
 * initialize the preview
 * 
 * @return string | boolean
 */
$formBuilder.prototype.startPreview = function() {
	var scope = angular.element($("#formWrapper")).scope();

	if(scope.data.name.length === 0 || scope.data.desc.length === 0 || scope.data.name == "" || scope.data.desc == "") {
		window.location = "#new";
		return false;
	}

	scope.saveForm(scope.data, 0, true);
	var formId = scope.formInformation.response.data;


	if(typeof(formId) != "undefined" && formId > 0) {
		return formId;
	} else {
		return false;	
	}
}

$formBuilder.prototype.backToEdit = function(formId) {
	var scope = angular.element($("#formWrapper")).scope();
}

/**
 * closes all windows
 * 
 * @return boolean
 */
$formBuilder.prototype.closeWindows = function() {
	var scope = angular.element($("#formWrapper")).scope();
		scope.showWindow = false;

	return true;
}

$formBuilder.prototype.calculateMobile = function() {
	var elements = {}, link_cache = {}, offset_cache = {};

	$('#formBuildGrid').children().each(function() {
		var self = $(this), id = self.attr('id'), position = self.position(), width = self.outerWidth(), height = self.outerHeight();
		if(id != "trashBin") {
			elements[id] = {
				id: id,
				position: position,
				offsets:position,
				width: width,
				height: height,
				x: [position.left, position.left + width],
				y: [position.top, position.top + height],
				right_of: null,
				below: null,
				linked: []
			};

			link_cache[position.top] = link_cache[position.top] || [];
			link_cache[position.top].push(id);
		}
	});

	$.each(elements, function() {
		var self = $(this)[0], top = null, left = null;

		$.each(elements, function() {
			var _self = $(this)[0];

			if(_self == self) {
				return;
			}
			
			var _x = (self.x[0] < _self.x[1] && self.x[1] > _self.x[0]),
				_y = (self.y[0] < _self.y[1] && self.y[0] >= _self.y[0]) || (_self.y[0] < self.y[1] && _self.y[0] >= self.y[0]);
							
			if(
				(
					self.x[0] >= _self.x[0] && self.x[0] <= _self.x[1]
				||
					self.x[1] >= _self.x[0] && self.x[1] <= _self.x[1]
				)
				&&
				(
					self.y[0] >= _self.y[0] && self.y[0] <= _self.y[1]
				||
					self.y[1] >= _self.y[0] && self.y[1] <= _self.y[1]
				)
			) {
				console.log(self.id + ' collides with ' + _self.id);
			}

			if(_y
				&& (
					left == null
						|| (
							left != null && (
								left.x[1] <= _self.x[1]  && _self.x[0] <= self.x[1]
							)
						)
					)
				&& _self.x[0] < self.x[1]
			) {
				left = _self;
				elements[self.id].right_of = _self.id;
				elements[self.id].offsets.left = self.x[0]- _self.x[1];
			} else if(
				_x
					&& (
						top == null
					|| (
						top != null && (
							top.y[1] < _self.y[1]
						|| (top.y[1] == self.y[1] && top.x[0] > _self.x[0])
						|| (top.y[1] <= self.y[1] && _y)
						)
					)
				)
				&& _self.y[0] < self.y[1]
			) {
				top = _self;
				elements[self.id].below = _self.id;
				elements[self.id].offsets.top = self.y[0]- _self.y[1];
			}

		});
	});

	$.each(elements, function() {
		var self = $(this)[0];
		console.log(self.id + ' is below ' + (self.below == null?'nothing':self.below) + ' and to the right of ' + (self.right_of == null?'nothing':self.right_of));
	});
}
