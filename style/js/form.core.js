/**
 * Core functions of the form builder
 *
 * Build with AngularJS, jQuery and vanilla Javascript
 *
 * (C) 2014 - Elogbooks LTD
 */

/**
 * initialize form class
 * 
 * @type class
 */
var form = new $formBuilder(typeof(window) !== "undefined"?window:"");

/**
 * set debug on or off with the console
 * 
 * @param boolaen
 */
var setDebug = function(val) {
	form.config.debug = typeof val != "boolean"?false:val;
	return "Debug mode set " + val;
}

/**
 * show config variable
 *
 * 
 */
var showConfig = function() {
	console.log("FORMBUILDER VERSION "+form.config.version);
	console.log("-------------------------------------------------");
	console.log("Author: "+form.config.details.author+" <"+form.config.details.email+">");
	console.log("--------------------------------------------------");
	console.log("Build: "+form.config.build);
	console.log("Build Date: "+form.config.date);
	console.log("Codename: "+form.config.codeName);
	console.log("Debug mode: "+form.config.debug);
	console.log("---------------------------------------------------");
	console.log("Custom Table:");
	console.log("Max Columns: "+form.config.tableConfig.custom.maxCols);
	console.log("Max Rows: "+form.config.tableConfig.custom.maxRows);
	console.log("Asset Table:");
	console.log("Max Columns: "+form.config.tableConfig.asset.maxCols);
	console.log("Max Rows: "+form.config.tableConfig.asset.maxRows);
	console.log("Widths: ");
	console.log("Custom Table: "+form.config.tableConfig.tableWidth);
	console.log("Asset Table: "+form.config.tableConfig.assetWidth);
	console.log("----------------------------------------------------");
	console.log("AutoSave: " + form.config.autoSaveOnOff);
	console.log("AutoSave Interval: " + form.config.autosave)
	console.log("----------------------------------------------------");
	console.log("setDebug("+(form.config.debug===false?'true':'false')+") to turn debug mode "+(form.config.debug===false?'on':'off')+"");
}

/**
 * write the footer
 *
 * 
 */
$(document).ready(function() {
	$("#formFooterContent").html('FormBuilder Version <strong>' + form.config.version + '</strong> <small><em>Build: ' + form.config.build + '</em></small>');
});

/**
 * initialize form class angular
 * 
 * @type class
 */
var app = angular.module("formApp", ['ngRoute', 'ngSanitize', 'angularFileUpload']);

/**
 * configure less
 * 
 * @type object
 */
var less = {
    env: "development",
    async: false,
    fileAsync: false,
    poll: 1000,
    functions: {},
    dumpLineNumbers: "comments",
    relativeUrls: false,
    rootpath: ":/"
};

/**
 * configure angular
 *
 * actually only the route provider
 */
app.config(function($routeProvider, $locationProvider) {
	$routeProvider.when('/preview/', {
	    controller: 	'formPreviewController',
	    templateUrl:    'tpl/preview.php'
	}).when('/new/', {
	    controller: 	'formController'
	}).when('/menu/', {
	    controller: 	'menuController',
	    templateUrl:    'tpl/menu.php'
	}).otherwise({
	    redirectTo: function() {
	    	return '/menu/'
	    }
	});
});

/** 
	create a compile directive to display html code via json

 */
app.directive('compile', ['$compile', function($compile) {
  return function(scope, element, attrs) {
    scope.$watch(
      function(scope) {
        return scope.$eval(attrs.compile);
      },
      function(value) {
        element.html(value);
        $compile(element.contents())(scope);
      }
   )};
}]);

/**
 * initialize the preview controller
 * 
 */
app.controller("formPreviewController", ['$scope', "$http", function($scope, $http) {
	$scope.overlay = false;
	$scope.formID = 0;
	$scope.alert = false;

	$scope.preview = function(mode) {
		$.when(form.startPreview()).done(function(data) { 
			var formId = data;
			if(typeof(formId) == "undefined" || typeof(formId) == "boolean" || formId === 0) {
				form.alertMessages("danger", "Error while trying to preview the form", "Failed to create preview, please save the form first!");
				window.location = '#new';
				return false;
			} else {
				$scope.overlay = true;
				$scope.load = false;
				$scope.formID = formId;

				$http({
					method: 'GET',
					url: '/api/data.php?1t&s=api&t=forms&q=previewForm&id=' + formId,
					cache: false
				})
				.success(function(data) {
					if(data.response.status.code === 200) {
						if(typeof(data.response.data.rows.jobs) != "undefined" && data.response.data.rows.jobs.length > 0) {
							$scope.jobs = data.response.data.rows.jobs;
						}
						if(form.renderPreviewForm(data.response.data, (typeof(mode) != "undefined"?mode:true))) {
							var css = {
								height: data.response.data.rows.info[0].grid_height,
								width: data.response.data.rows.info[0].grid_width
							};

							$("#formBuildGridPreview").css(css);
							var h_ = $("#formBuildGridPreview").css("height");
								h_ = h_.split("px");
								h_ = parseInt(h_[0]) + 70 + 34;
							var css_ = {
								height: h_
							}
							$("#overlay").css(css_);
						}
					} else {
						form.alertMessages('danger', 'Error', data.response.message);
					}
				})
				.error(function(data) {

				});
			}
		});

	}

	$scope.preview();

	$scope.previewForm = function(jobId) {
		if(typeof(jobId) == "undefined" || jobId == "") {
			form.alertMessages("danger", "Error", "Please enter a Job ID to use this function");
			$scope.alert = true;
			return false;
		}

		$scope.overlay = true;
		$scope.load = false;

		$("#formBuildGridPreview").html('<div style="width: 300px; margin-left: auto; margin-right: auto; padding-top: 50%;"><div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped active"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%"><span><strong>Loading form</strong></span></div></div></div>');

		$http({
			method: 'GET',
			url: '/api/data.php?1t&s=api&t=forms&q=previewForm&id=' + $scope.formID + '&job='+jobId,
			cache: false
		})
		.success(function(data) {
			if(data.response.status.code === 200) {
				if(form.renderPreviewForm(data.response.data, true, 'job')) {
					$scope.alert = false;
					var css = {
						height: data.response.data.rows.info[0].grid_height,
						width: data.response.data.rows.info[0].grid_width
					};

					$("#formBuildGridPreview").css(css);
					var h_ = $("#formBuildGridPreview").css("height");
						h_ = h_.split("px");
						h_ = parseInt(h_[0]) + 70 + 34;
					var css_ = {
						height: h_
					}
					$("#overlay").css(css_);
				}
			} else {
				form.alertMessages('danger', 'Error', data.response.message);
				$scope.preview(false);
			}
		})
		.error(function(data) {

		});
	}

	$scope.backToEdit = function() {
		form.backToEdit($scope.formId);
	}

}]);

/**
 * initialize the menu controller
 *
 */
app.controller("menuController", ['$scope', "$http", function($scope, $http) {
	$scope.formLoad = [];
	$scope.load = true;
	$scope.onLoad = true;
	$scope.dataLimit = 0;
	$scope.noforms = false;
	$scope.restore = false;

	form.closeWindows();

	$scope.loadPages = function(page) {
		$scope.formLoad = [];
		$scope.onLoad = true;
		$scope.dataLimit = page;
		$http({
	        xhr: function() {
	            var xhr = new window.XMLHttpRequest();
	            xhr.addEventListener('progress', function(e) {
	                if (e.lengthComputable) {
	                	console.log(100 * e.loaded / e.total);
	                    $('.progress-bar-striped .active').css('width', '' + (100 * e.loaded / e.total) + '%');
	                }
	            });
	            return xhr;
	        },
			method: 'GET',
			url: '/api/data.php?1t&s=api&t=forms&q=forms&limit=' + $scope.dataLimit,
			cache: false
		})
		.success(function(data) {
			if(data.forms == '0') {
				$scope.noforms = true;
			}
			$scope.formLoad.push(data);
			$scope.onLoad = false;
		})
		.error(function(data) {
			form.devLog("error");
		});
	}

	$scope.loadPages(0);

	$scope.newForm = function() {
		$("#formBuildGrid").html('<div id="trashBin"><div id="trashContent"><span class="glyphicon glyphicon-trash" style="font-size: 48px;"></span></div></div>');
		
		$("#trashBin").droppable({
			tolerance: 'pointer',
			drop: function(event, ui) { form.deleteBox(event, ui); }
		});

		if(form.createNewForm()) {
			window.location = '#/new';
		}
	};

	$scope.getForm = function(id, restore) {
		$scope.load = false;
		$http({
			method: 'GET',
			url: '/api/data.php?1t&s=api&t=forms&q=form&id=' + id + (typeof(restore) == "undefined"?'':"&restore=true"),
			cache: false
		})
		.success(function(data) {
			if(data.response.status.code === 200) {
				if(form.renderForm(data.response.data)) {

					var css = {
						height: data.response.data.rows.info[0].grid_height,
						width: data.response.data.rows.info[0].grid_width
					};
					var css_ = {
						height: $(window).height()
					}

					$("#formBuildGrid").css(css);
					$("#formWrapper").css(css_);

					window.location = "#/new";
				}
			} else {
				form.alertMessages('danger', 'Error', data.response.status.mes);
			}
		})
		.error(function(data) {

		});
	};

}]);

/**
 * initialize the form controller
 * 
 */
app.controller("formController", ["$scope", "$http", "FileUploader", "Utils", function($scope, $http, FileUploader, Utils) {
	$scope.formInformation = [];		//
	$scope.formInformationCache = [];	//	FORM INFORMATIONS
	$scope.data = {name: '', desc: ''};	//

	$scope.windowTpl = "img";			// Standard window template
	$scope.showWindow = false;			// window open?

	$scope.saving = false;				// show saving screen
	$scope.delete = false;				// show deleting screen
	$scope.autosaving = false;			// autosave

	$scope.contentId = 0;				// current div id
	$scope.content = "";				// current div content

	$scope.customeTableView = true;		// change the view in the table template	

	$scope.ui = "";						// ui cache
	$scope.event = "";					// event cache

	$scope.pictures = [];				// picture cache

	$scope.asset = {};					// asset cache
	$scope.alreadyCreated = false;		// already created an asset table?
	$scope.assetId = "";				// current asset table 
	$scope.assetsCheck = {status: false, model: false, serial: false, notes: false, name: false, desc: false, loca: false, input: false, check: false}; // values for editing the table

	$scope.variableList = [];			// all variables

	$scope.dropdown = []; 				// dropdown elements
	$scope.drop = "";					// drop input to clear it after adding a drop
	$scope.createDrop = false;			// hide some fields
	$scope.dropdownLabel = "";			// Label for the dropdown
	$scope.dropdownId = "";				// current dropdown id

	$scope.tableName = "";				// current table name
	$scope.tableId = "";				// current table id

	// init the image uploader
	var uploader = $scope.uploader = new FileUploader({url: 'upload.php'});

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
		if(response.status.code === 200) {
			form.alertMessages("success", "Success", response.message);
			$scope.createImage(response.file);
		} else {
			form.alertMessages("danger", "Error", response.message);
		}
		uploader.queue = [];
	};

	$scope.addDrop = function(drop) {
		if(typeof(drop) != "undefined" && drop != "") {
			var drop_count = 0;
			for(b in $scope.dropdown) {
				if($scope.dropdown[b] == drop) {
					return false;
				}
				drop_count++;
			}
			drop_count += 1;

			var obj = {};
				obj["drop_"+drop_count] = drop;

			$scope.dropdown.push(obj);
			$scope.drop = "";

			return true;
		} else {
			return false;
		}
	}

	$scope.deleteDrop = function(key) {
		if(typeof(key) != "undefined") {
			var getIndex = function(array, key_c) {
				for(var i = 0; i < array.length; i++) {
					if(array[i].hasOwnProperty(key_c)) {
						return i;
					}
				}
				return -1;
			}

			$scope.dropdown.splice(getIndex($scope.dropdown, key), 1);
		}
	}

	$scope.saveDropdown = function() {
		if($scope.dropdown.length > 0) {
			if($scope.dropdownId != "") {
				if(form.updateDropDown($scope.dropdown, $scope.dropdownId)) {
					$scope.showWindow = false;
					$scope.dropdownId = "";
					return true;
				}

				return false;
			} else {
				if(form.createDropdown($scope.dropdown, $scope.dropdownLabel)) {
					$scope.showWindow = false;
					return true;
				}
				
				return false;
			}
		} else {
			form.alertMessages("danger", "Error", "Can't save empty dropdown");
			return false;
		}
	}

	$scope.saveEditTable = function(table) {
		if(table != "") {
			$scope.tableName = table;
		} else return false; 

		if(form.saveCustomTable($scope.tableId, $scope.tableName)) {
			$scope.showWindow = false;
		}
	}

	$scope.getVariables = function() {
		$http({
			method: 'GET',
			url: '/api/data.php?1t&s=api&t=forms&q=variables',
			cache: false
		})
		.success(function(data) {
			$scope.variableList = data;
		})
		.error(function(data) {
			form.alertMessages('danger', 'Error', 'Failed to load form informations');
		});
	}

	$scope.getVariables();

	$scope.maxAssets = function() {
		var count = 0;
		for(var i in $scope.asset) {
			if($scope.asset[i] == true) {
				count++;
			}
		}

		return (count===form.config.tableConfig.asset.maxCols ? true : false);
	}

	$scope.safeApply = function(fn) {
	  var phase = this.$root.$$phase;
	  if(phase == '$apply' || phase == '$digest') {
	    if(fn && (typeof(fn) === 'function')) {
	      fn();
	    }
	  } else {
	    this.$apply(fn);
	  }
	};

	$scope.closeWindow = function() {
		$scope.showWindow = false;
	}

	$scope.popupWindow = function(tpl) {
		$scope.windowTpl = tpl;
		$scope.showWindow = true;
	}

	$scope.createImage = function(value) {
		if(typeof(value) == "undefined") {
			form.alertMessages("danger", "Error", "No img source");
			return false;
		}

		if($scope.ui.length === 0) {
			if(form.createImageBox(value, 0, 0, true)) {
				$scope.showWindow = false;
			} else {
				alert("Failed to put the image in");
			}
		} else {
			if(form.createImageBox(value, $scope.event, $scope.ui)) {
				$scope.showWindow = false;
				$scope.event = "";
				$scope.ui = "";
			} else {
				alert("Failed to put the image in");
			}
		}
	}

	$scope.saveVariable = function(input) {
		if(typeof(input) == "undefined") {
			form.alertMessages("danger", "Error", "Can't create empty variable");
			return false;
		}
		if(form.createNewVariable(input)) {
			$scope.showWindow = false;
		}
	}

	$scope.saveTable = function(input) {
		if(typeof(input) == "undefined") {
			form.alertMessages("danger", "Error", "Can't create empty table");
			return false;
		}

		if(form.createNewTable(input.name, input.cols, input.rows, input.thdata, input.coldata)) {
			$scope.showWindow = false;
		}
	}

	$scope.createAssetTable = function(assets) {
		if(assets.length === 0) {
			form.alertMessages("danger", "Error", "Can't create empty table");
			return false;
		} else {
			if(form.createAssetTable(assets)) {
				$scope.showWindow = false;
				$scope.asset = {};
				$scope.alreadyCreated = true;
			}
		}
	}

	$scope.updateAssetTable = function(assets) {
		if(typeof(assets) == "undefined" || assets.length === 0) {
			form.alertMessages("danger", "Error", "Can't create empty table");
			return false;
		} else {
			if(form.updateAssetTable(assets, $scope.assetId)) {
				$scope.showWindow = false;
			}
		}
	}

	$scope.saveForm = function(data, error, preview, autoupdate) {
		form.devLog("Start saving..");
		if(data.name == "") {
			form.alertMessages('danger', 'Error', 'Please give the form a name');
			return false;
		}
		if(data.desc == "") {
			form.alertMessages('danger', 'Error', 'Please give the form a description');
			return false;
		}
		if(typeof(autoupdate) == "undefined" || autoupdate === false) {
			$scope.saving = true;
		} else {
			$scope.autosaving = true;
		}
		$scope.formSaved = [];
		$scope.formBoxes = boxes;

		$scope.formSaved.push({formData: {formId: $scope.formInformation.response.data, formName: data.name, formDesc: data.desc}});

		$("#formBuildGrid").children().each(function() {
			var self = $(this);

			var sqlId = self.data('information');
			var type = self.data("type");

			if(type == "image") {
				$(this).children().each(function() {
					var self_ = $(this);
					var src   = self_.attr("src");
					if(src != "" && typeof(src) != "undefined") {
						Utils.isImage(src).then(function(result) {
							if(result === false) {
								console.log("Deleted Image: " + self_.attr("id"));
								self.remove();
								self_.remove();
							}
						});
					}
				});
			}

			var id = self.attr("id");
			var classes = self.attr("class");
			var positions = {x: self.position().left, y: self.position().top};
			var dim = {width: self.outerWidth(), height: self.outerHeight()};

			switch(type) {
				case "whitebox":
					var content = $("#"+id + " > .whiteBoxContent").html();
					if(content.length === 0) {
						content = "\n";
					}
				break;
				case "customTable":
					var content = self.html();
						content = content.split('<div class="hoverMenu">');
						content = content[0];
				break;
				default:
					var content = self.html();
				break;
			}

			if(typeof(content) != "undefined" && content != "") {
				var push = {box: {id: id, classes: classes, positions: positions, dimension: dim, content: content, sqlId: sqlId, type: type}};
			}
			form.devLog("Collected data of " + id);
			$scope.formSaved.push(push); 
		});

		var grid_height = $("#formBuildGrid").outerHeight();
		var grid_width = $("#formBuildGrid").outerWidth();

		$scope.formSaved.push({grid: {height: grid_height, width: grid_width}});
		
		var data_save = {data: $scope.formSaved, preview: (preview===true?true:false)};

		$http({
			method: 'POST',
			url: '/api/data.php?1t&s=api&t=forms&q=saveForm',
			data: data_save,
			cache: false
		})
		.success(function(data) {
			form.devLog("Saved!");
			$scope.saving = false;
			if(typeof(autoupdate) != "undefined" || autoupdate === true) {
				console.log("Autosaved -- " + Date.now());
				$scope.autosaving = false;
				return true;
			}
			if(error == '1') {
				if(data.response.status.code === 200) {
					form.alertMessages('success', 'Success', data.response.message);
				} else {
					form.alertMessages('danger', 'Error', data.response.message);
					window.location = '#new';
				}
			} else {
				return true;
			}
		})
		.error(function(data) {
			$scope.saving = false;
			form.alertMessages('danger', 'Error', 'Failed to save the form');
			return false;
		})
	}

	$scope.saveContent = function(input) {
		if(form.saveWhiteContent(input, $scope.contentId)) {
			$scope.showWindow = false;
		}
	}

	$scope.saveInputType = function(input) {
		if(typeof(input) == "undefined") {
			form.alertMessages("danger", "Error", "Can't create empty input");
			return false;
		}

		var value = typeof input.value == "undefined"?"test":input.value;

		if(form.createInputField(input.check, input.label, value)) {
			$scope.showWindow = false;
		}
	}

	$scope.changeFormId = function() {
		$scope.formInformation = [];
		$http({
			method: 'GET',
			url: '/api/data.php?1t&s=api&t=forms&q=info',
			cache: false
		})
		.success(function(data) {
			$scope.formInformation = data;
			$scope.formInformationCache = data;
			$scope.getImages();
		})
		.error(function(data) {
			form.alertMessages('danger', 'Error', 'Failed to load form informations');
		});
	}

	$scope.changeFormId();

	$scope.getImages = function() {
		$http({
			method: 'GET',
			url: 'get.php?formid=' + $scope.formInformation.response.data,
			cache: false
		})
		.success(function(data) {
			$scope.pictures = data.data;
		})
		.error(function(data) {
			form.alertMessages('danger', 'Error','Failed to load images');
		});
	}

	$scope.deleteImage = function(pic) {
		$("#formBuildGrid").children().each(function() {
			var self = $(this);
			var type = self.data("type");
			var img  = pic;
				img  = img.split(".");
				img  = img[0]+'.'+img[2];

			if(type == "image") {
				$(this).children().each(function() {
					var self_ = $(this);
					var src   = self_.attr("src");

					if(src != "") {
						if(src == img) {
							self.remove();
							self_.remove();
						}
					}
				});
			}
		});

		$http({
			method: 'GET',
			url: 'get.php?delete=' + pic +';'+$scope.formInformation.response.data,
			cache: false
		})
		.success(function(data) {
			$scope.pictures = data.data;
		})
		.error(function(data) {
			form.alertMessages('danger', 'Error', 'Failed to delete image');
		});
	}

	$scope.deleteForm = function() {
		$scope.delete = true;
		$http({
			method: 'POST',
			url: '/api/data.php?1t&s=api&t=forms&q=deleteForm&id='+$scope.formInformation.response.data,
			cache: false
		})
		.success(function(data) {
			if(typeof(data.response.status) != "undefined") {
				if(data.response.status.code === 200) {
					form.alertMessages('success', 'Success', data.response.message);

					$scope.data.name = "";
					$scope.data.desc = "";

					window.location = '#/menu';

					$("#formBuildGrid").html('<div id="trashBin"><div id="trashContent"><span class="glyphicon glyphicon-trash" style="font-size: 48px;"></span></div></div>');
					
					$("#trashBin").droppable({
						tolerance: 'pointer',
						drop: function(event, ui) { form.deleteBox(event, ui); }
					});

					$scope.delete = false;
				} else {
					form.alertMessages('danger', 'Error', data.response.status.mes + " | " + data.response.message);
					
					$scope.delete = false;
				}
			} else {
				form.alertMessages('danger', 'Error', "Can't delete an empty form");
					
				$scope.delete = false;
			}
		})
		.error(function(data) {
			form.alertMessages('danger', 'Error', 'Failed to delete the form');
		});
	}

	$scope.test = function() {
		form.calculateMobile();
	}

	$scope.autoSave = function() {
		if($scope.data.name == "") {
			return false;
		}
		if($scope.data.desc == "") {
			return false;
		}

		$scope.saveForm($scope.data, 0, true, true);
	}

	if(form.config.autoSaveOnOff === true) {
		setInterval(function() {
			$scope.autoSave();
		}, form.config.autosave); //120000
	}

}]);

app.factory('Utils', function($q) {
    return {
        isImage: function(src) {
        
            var deferred = $q.defer();
        
            var image = new Image();

            image.onerror = function() {
                deferred.resolve(false);
            };
            image.onload = function() {
                deferred.resolve(true);
            };
            image.src = src;
        
            return deferred.promise;
        }
    };
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.filter('partition', function() {
	var cache = {};
	var filter = function(arr, size) {
		if(!arr) { return; }
		var newArr = [];
		for(var i=0; i<arr.length; i+=size) {
      		newArr.push(arr.slice(i, i+size));
    	}
		var arrString = JSON.stringify(arr);
		var fromCache = cache[arrString+size];
		if(JSON.stringify(fromCache) === JSON.stringify(newArr)) {
			return fromCache;
		}
		cache[arrString+size] = newArr;
		return newArr;
  	};

  	return filter;
});
