<?php
session_start();

function print_e($value) {
	echo "<pre>";
	print_r($value);
	echo "</pre>";
}

if(!empty($_FILES)) {
	$tmp = $_FILES["file"]["tmp_name"];

	if(empty($_SESSION["FORM::FORM_ID::AUTO"])) {
		$upload = "uploads/".$_FILES["file"]["name"];
		return;
	} else {
		$allowed = array("jpg", "png", "gif");

		$file = $_FILES["file"]["name"];
		$type = explode(".", $file);

		foreach($type as $val) {
			if(in_array($val, $allowed)) {
				$file_type = $val;
				break;
			}
		}

		if(empty($file_type)) {
			echo json_encode(array("status" => array("code" => 406, "mes" => "Not Acceptable"), "message" => "No allowed file type"));
			return;
		}

		if($_FILES["file"]["size"] > 480000) {
			echo json_encode(array("status" => array("code" => 406, "mes" => "Not Acceptable"), "message" => "File is too big"));
			return;
		}

		$file = md5($file);
		$file = substr($file, 0, 20);
		$file = $_SESSION["FORM::FORM_ID::AUTO"]."_".$file;
		$thumb = $file.".thumb.".$file_type;
		$t_upload = "uploads/".$thumb;
		$upload = "uploads/".$file.".".$file_type;
	}
	
	if(move_uploaded_file($tmp, $upload)) {
		if(makeThumb($upload, $thumb)) {
			echo json_encode(array("status" => array("code" => 200, "mes" => "OK"), "message" => "Uploaded successful", "file" => $upload));
		} else {
			unlink($upload);
			echo json_encode(array("status" => array("code" => 406, "mes" => "Not Acceptable"), "message" => "Creating thumbnail failed"));
		}
	} else {
		echo json_encode(array("status" => array("code" => 406, "mes" => "Not Acceptable"), "message" => "Upload failed"));
	}
} else {
	echo json_encode(array("status" => array("code" => 406, "mes" => "Not Acceptable"), "message" => "No allowed file type"));
}

function makeThumb($img = NULL, $name = NULL) {
	if(!is_null($img)) {
		if(!is_null($name)) {
			$img_c = imagecreatetruecolor(60, 50);
			list($width, $height) = getimagesize($img);
			$type = explode(".", $img);
			$file_type = strtolower($type[1]);
			switch($file_type) {
				case "jpg":
					$img_ = imagecreatefromjpeg($img);
				break;
				case "png":
					$img_ = imagecreatefrompng($img);
				break;
				case "gif":
					$img_ = imagecreatefromgif($img);
				break;
				default:
					return false;
				break;
			}
			imagecopyresampled($img_c, $img_, 0, 0, 0, 0, 60, 50, $width, $height);

			imagejpeg($img_c, "uploads/".$name, 100);

			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}
