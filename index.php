<!DOCTYPE html>
<html lang="en">
	<head>
		<title>FORM BUILDER UI</title>
		<link rel="stylesheet" href="style/css/bootstrap.min.css">
		<link rel="stylesheet" href="style/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="style/jquery-ui.min.css">
		<link rel="stylesheet/less" href="style/css/style.less">
		<script type="text/javascript" src="style/js/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="style/jquery-ui.min.js"></script>
		<script type="text/javascript" src="style/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="style/js/angular.min.js"></script>
		<script type="text/javascript" src="style/js/sanitize.js"></script>
		<script type="text/javascript" src="style/js/angular-file-upload.min.js"></script>
		<script type="text/javascript" src="style/js/route.js"></script>
		<script type="text/javascript" src="style/js/jquery.jeditable.mini.js"></script>
		<script type="text/javascript" src="style/js/form.boxes.js?00700"></script>
		<script type="text/javascript" src="style/js/form.core.js?00700"></script>
		<script type="text/javascript" src="style/js/less-1.7.4.min.js"></script>
	</head>
	<body ng-app="formApp">
		<div id="formWrapper" ng-controller="formController">
			<div id="autosave" ng-if="autosaving">
				Autosaving..
			</div>
			<div id="overlay" ng-if="delete">
				<div id="overlayContainer">
					<div id="overlayHead">
						Deleting the form
					</div>
					<div id="overlayContent">
						Deleting..
					</div>
				</div>
			</div>		
			<div id="overlay" ng-if="saving">
				<div id="overlayContainer">
					<div id="overlayHead">
						Saving the form
					</div>
					<div id="overlayContent">
						Saving..
					</div>
				</div>
			</div>
			<div ng-view></div>
			<div id="windowAlerts">
				<div id="waContent"></div>
			</div>
			<div id="windowBoxes" ng-if="showWindow === true">
				<div ng-include="'tpl/' + windowTpl + '.php'"></div>
			</div>
			<div id="formTopBar">
				<div id="formTopBarContent">
					<button ng-click="test()" class="form-ui-button">Test</button>
					<button data-toggle="modal" data-target="#formHelp" class="form-ui-button"><span class="glyphicon glyphicon-question-sign"></span> Help</button>
					<button onclick="self.location.href='#menu'" class="form-ui-button"><span class="glyphicon glyphicon-th-list"></span> Menu</button>
					<button onclick="self.location.href='#preview'" class="form-ui-button"><span class="glyphicon glyphicon-eye-open"></span> Preview Form</button>
					<button class="form-ui-button" ng-click="saveForm(data, 1)" class="form-ui-button" ng-disabled="saving"><span class="glyphicon glyphicon-floppy-disk"></span> Save Form</button>
					<button class="form-ui-button" ng-click="deleteForm()"><span class="glyphicon glyphicon-remove"></span> Delete Form</button>
				</div>
			</div>
			<div class="modal fade" id="formHelp" tabindex="-1" role="dialog" aria-labelledby="formHelp" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			        		<h4 class="modal-title">Form Builder Help</h4>
						</div>
						<div class="modal-body">
							<h4>Variables:</h4>
							<hr />
							Variables are pre defined from the system and will be replaced in the job with the job content.
							Example usage:<br /><br />
							<code><br />Customer Name: {customerName} // Will display: Customer Name: A Name<br />Engineer: {engineer} // Will display: Engineer: Max Mustermann</code><br /><br />
							You can define them in a whitebox, a table or in a standalone variable.<br />(Please remind the asset variables wont work out of the table)<br /><br />
							<h4>Whitebox:</h4>
							<hr />
							A white box is a simple white box. You can put your own content in or just display it as a gap.<br /><br />
							<h4>Tables:</h4>
							<hr />
							You can choose between a sub asset table or a custom table. Sub asset tables are predefined and can't be change, but a custom table can you
							define how you want.<br /><br />
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div id="formBuilder">
				<div id="formBuilderLeft">
					<div class="widgets">
						<div class="formWidgetContainer">
							<div class="formWidgetContainerHead">
								Form Details
							</div>
							<div class="formWidgetContainerContent">
								<div class="col-xs-6 col-md-4">
									Form Name:
								</div>
								<div class="col-xs-6 col-md-4">
									<input type="text" ng-model="data.name" class="form-ui-input"/>
								</div>
								<br style="clear: left;" />
								<br />
								<div class="col-xs-6 col-md-4">
									Description:
								</div>
								<div class="col-xs-6 col-md-4">
									<input type="text" ng-model="data.desc" class="form-ui-input" />
								</div>
								<br style="clear: left;" />
								<br />
								<div class="col-xs-6 col-md-4">
									Form ID:
								</div>
								<div class="col-xs-6 col-md-4">
									<span ng-if="formInformation.response">{{formInformation.response.data}}</span>
									<span ng-if="!formInformation.response">Loading..</span>
								</div>
								<br style="clear: left;" />
							</div>
						</div>
						<div class="formWidgetContainer">
							<div class="formWidgetContainerHead">
								Add Widgets to the grid
							</div>
							<div class="formWidgetContainerContent" style="padding-top: 12px;">
								<br />
								<div id="ab" class="btn btn-default btn-lg ab" style="z-index: 910;">
		  							<span class="glyphicon glyphicon-plus-sign"></span>
								</div>
								<div id="im" class="btn btn-default btn-lg im" style="z-index: 910;" ng-click="getImages()">
		  							<span class="glyphicon glyphicon-picture"></span>
								</div>
								<button type="button" class="btn btn-default btn-lg" ng-click="popupWindow('table')">
		  							<span class="glyphicon glyphicon-th-list"></span>
								</button>
								<button type="button" class="btn btn-default btn-lg" ng-click="popupWindow('input')">
		  							<span class="glyphicon glyphicon-pencil"></span>
								</button>
								<button type="button" class="btn btn-default btn-lg" ng-click="popupWindow('variable')">
		  							<span class="glyphicon glyphicon-usd"></span>
								</button>
							</div>
						</div>
					</div>
					<div id="formFooter">
						<div id="formFooterContent"></div>
					</div>
				</div>
				<div id="formBuilderRight">
					<div id="formBuildGrid">
						<div id="trashBin">
							<div id="trashContent">
								<span class="glyphicon glyphicon-trash" style="font-size: 48px;"></span>
							</div>
						</div>
					</div>
				</div>
				<br style="clear: left;">
				<br />
			</div>
			<div id="contextMenu">
				<ul class="list-group">
					<a href class="list-group-item">Delete Row</a>
					<a href id="deleteColumn" class="list-group-item">Delete Column</a>
					<a href id="deleteTable" class="list-group-item">Delete Table</a>
				</ul>
			</div>
			<div id="contextMenuHead">
				<ul class="list-group">
					<a href id="deleteColumnH" class="list-group-item">Delete Column</a>
					<a href id="deleteTableH" class="list-group-item">Delete Table</a>
				</ul>
			</div>
		</div>
	</body>
</html>
