<?php

$get = $_GET;

if(!empty($get)) {
	if(!empty($get["formid"])) {
		if(is_numeric($get["formid"])) {
			$id = $get["formid"];

			getImages($id);
		}
	} elseif(!empty($get["delete"])) {
		$delete = explode(";", $get["delete"]);

		if(file_exists($delete[0])) {
			if(unlink($delete[0])) {
				$big = explode(".", $delete[0]);
				$big = $big[0].".".$big[2];
				if(unlink($big)) {
					getImages($delete[1], TRUE);
				} else {
					echo json_encode(array("status" => array("code" => 200, "mes" => "OK"), "message" => "Image couldn't be deleted"));
				}
			} else {
				echo json_encode(array("status" => array("code" => 200, "mes" => "OK"), "message" => "Image couldn't be deleted"));
			}
		} else {
			echo json_encode(array("status" => array("code" => 200, "mes" => "OK"), "message" => "No image found"));
		}

		
	}
}

function getImages($formId, $del = FALSE) {
	$handle = opendir("uploads/");

	$allowed = array("jpg", "png", "gif");

	while($file = readdir($handle)) {
		if($file != "." && $file != "..") {
			$file_ = explode("_", $file);
			if(!empty($file_[1])) {
				$type = explode(".", $file_[1]);
				if(in_array($type[1], $allowed)) {
					if($file_[0] == $formId) {
						$push[] = "uploads/".$file;
						$thumb = explode(".", $file);
						$push[]["thumb"] = "uploads/".$thumb[0].".thumb.".$thumb[1];
					}
				}
			}
		}
	}
	if(!empty($push)) {
		if($del === FALSE) {
			echo json_encode(array("status" => array("code" => 200, "mes" => "OK"), "data" => $push));
		} else {
			echo json_encode(array("status" => array("code" => 200, "mes" => "OK"), "data" => $push, "message" => "Image removed"));
		}
	} else {
		if($del === FALSE) {
			echo json_encode(array("status" => array("code" => 200, "mes" => "OK"), "message" => "No images for this form"));
		}
	}
}
