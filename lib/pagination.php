<?php

error_reporting(0);

/**
 * pre version of print_r
 * @param  string $val
 * @return pre print_r code
 **/
function print_e($val) {
	echo "<pre>";
	print_r($val);
	echo "</pre>";
}

/**
 * Changelog:
 *   
 * Version: 1.0.1.1 (Minor Update)
 * - Update General
 *   # Added versions variable
 *   # Added array classnames
 *     - Now you can change the class names of the pagination outside the class code
 * - Added function push_array:
 *   # You can add now elements to an array
 * - Changed function change_signs to change_array
 *   # You are now able to change any array instead of only signs
 * - Update function create_pagination:
 *   # Now gives an correct array of next and current page
 *   
 * Version: 1.0.1.0 (Major update)
 * - Update General
 *   # Declared the variables types as private and public, now you can only access to a few public variables outside of the class
 *   # Added an array signs for the special signs for first page, previous page, next page and last page
 * - Update functions name to create_list and render_pagination
 * - Update function __construct
 *   # Added variable method
 *     - To change the get method of the pagination (e.g. ?p or ?pagination) | Default will be pagination
 *   # Deleted auto render function
 *   # Removed maxpages
 *     - max_pages is now a private variable and can't be changed anywhere (unless i find a way to fix the counting)
 * - Update function render_pagination 
 *   # Added variable query
 *     - With this variable you can add a url query before the pagination (e.g. index.php?test=testing&p=200)
 * - Added function create_pagination
 *   # Returns an array with a formatted pagination
 *     - So now you can create your own design for the pagination while using the array
 *     - Usage: $obj = $pagination->create_pagination();
 *     -		print $obj->first["output"];
 *     -		print $obj->previus["output"];
 *     -		foreach($obj->next as $next) {
 *     -			print $next["output"];
 *     -		}
 *     -		print $obj->nextpage["output"];
 *     -		print $obj->last["output"];
 *     -	It only displays now the output! (Pseudo)
 *     -	To get the values of the pages use ["value"];
 * - Added function change_signs
 *   # Changes the signs array
 *     - Usage: $var->change_signs(array("first" => "F"));
 *  
 * 
 * Version: 1.0.0.3 (Hot Fix)
 * -Update function createList:
 * 	# Added a security check for $_GET "Pagination", needs now to be numeric
 * -Update function renderPagination:
 * 	# Added a title to the div box. So the pages will show up as a "tool tip"
 *
 * Version: 1.0.0.2 (Minor Update)
 * -Update function createList:
 * 	# Fixed counting for maxpages at the end shows now always 7 pages
 * -Update renderPagination:
 * 	# Added a style to the HTML Output and VAR Output
 * 		-- can be styled with #PaginationContent for the DIV Container, .pg for the links and .pga for the current site
 *
 * Version: 1.0.0.1 (Hotfix)
 * -Update function createList:
 *  # Fixed counting for maxpages in the beginning shows now always 7 pages
 *
 * Version: 1.0.0.0 (Major)
 * Created the class
 */

/**
 * @author  Tobias Fuchs <saikon@hotmail.de>
 *
 * class Pagination version 1.0.1.1
 * Creates and Renders a dynamic pagination list
 *
 * No need of sql, just the amount of rows (e.g. 200)
 *
 * Output: HTML & VAR (html = echo | var = return)
 *
 * @var int rows // rows amount
 * @var int perpage // amount of results per page
 * @var int maxpages // max pages shown in the pagination
 * @var bool debug // debug mode (more like print $this)
 *
 * Pagination URL's will be at first e.g. "?pagination=20" but can be changed with $method (watch changelog)
 *
 * Usage example:
 * $pagination = new Pagination(3125); // amount of rows
 * $pagination->renderPagination("html"); // output
 *
 * $pagination->renderPagination("var", "&orderby=DESC"); // add an extend to extend your url
 *
 * get information for the sql query with "$pagination->limit" and "$pagination->limitend" or "$pagination->perpage"
 */
class Pagination {

	/**
	 * $version
	 * @var  string
	 */

	private $version = '1.0.1.1';

	/**
	 * $perpage results per page
	 * @var integer
	 */
	public $perpage  = 5; // default value

	/**
	 * $maxpages max. pages shown in the pagination
	 * @var integer
	 */
	private $maxpages = 3; // default value

	/**
	 * $rows amount of rows
	 * @var integer
	 */
	public $rows = 0; // default value

	/**
	 * $pages amount of pages
	 * @var integer
	 */
	public $pages = 0;

	/**
	 * $pagenow includes the current page
	 * @var integer
	 */
	private $pagenow = 0;

	/**
	 * $limit first limit of the mysql query
	 * @var integer
	 */
	public $limit = 0;

	/**
	 * $limitend second limit for the sql query
	 * @var integer
	 */
	public $limitend = 0;

	/**
	 * $new_pages_output includes all the new pages
	 * @var array
	 */
	private $new_pages_output = array();

	/**
	 * $output includes the pagination code
	 * @var string
	 */
	private $output;

	/**
	 * $last_page data of the last page array
	 * @var array
	 */
	private $last_page = array();

	/**
	 * $pages_list includes new pages limits
	 * @var array
	 */
	private $pages_list = array();

	/**
	 * includes the current page
	 * @var interger
	 */
	private $current_page = 0;
	
	/**
	* $method
    * @var string
    */
	private $method = "pagination";

	/**
	 * $signs
	 * @var array
	 */
	private $signs = array(
		"first" => "&laquo;",
		"previous" => "&lsaquo;",
		"nextpage" => "&rsaquo;",
		"last" => "&raquo;"
	);

	/**
	 * $classname
	 * @var  string
	 */
	public $classnames = array(
		"class" => "pg", 
		"active" => "pga",
		"pagination" => "PaginationContent"
	);

	/**
	 * __construct
	 * @param int     $rows     amount of rows
	 * @param int     $perpage  results per page
	 * @param int     $maxpages max. pages shown per page
	 * @param boolean $debug    debug mode (true = on)
	 */
	public function __construct($rows=NULL, $perpage=NULL, $current=NULL, $debug=FALSE) {
		if(!empty($rows)) {
			$this->rows = $rows;
		} else {
			return FALSE;
		}
		if(!empty($method)) $this->method = (string)$method;

		$this->perpage = (!$perpage ? $this->perpage : $perpage);

		$this->pagenow = ($current == 0 ? 0 : $current);


		$this->limit = (int)(!$this->pagenow ? 0 : $this->pagenow);
		$this->limitend = (int)(!$this->pagenow ? $this->perpage : $this->pagenow+$this->perpage);

		$this->pages = ceil($this->rows / $this->perpage);

		$this->create_list();

		if(function_exists('print_e')) {
			if((bool)$debug) print_e((object)$this);
		} else {
			if((bool)$debug) print_r((object)$this);
		}

		return $this;
	}

	/** 
	 * change_signs
	 * changes the signs array
	 * @return  bool
	 */
	public function change_array($name=null, $new=array()) {

		$array = $this->$name;
		if(!is_array($array)) return FALSE;

		if(is_array($new)) {
			foreach($new as $key => $value) {
				if(array_key_exists($key, $array)) {
					$array[$key] = $value;
				} else {
					return FALSE;
				}
			}
		} else {
			return FALSE;
		}

		$this->$name = $array;

		return TRUE;
	}

	/** 
	 * push_array
	 * push elements into an array
	 * @return  bool
	 **/
	public function push_array($name=null, $value=array()) {

		$array = $this->$name;
		if(!is_array($array)) return FALSE;

		if(is_array($value)) {
			foreach($value as $key => $val) {
				if(array_key_exists($key, $array)) {
					return FALSE;
				} else {
					$array[$key] = $val;
				}
			}
		} else {
			return FALSE;
		}

		$this->$name = $array;

		return TRUE;
	}

	/**
	 * create_list
	 * @return array includes all new pages
	 */
	private function create_list() {
		$list = 0; // list starts at 0
		for($i=1; $i<((int)$this->pages+2); $i++) { 
			if($i != 1) {
				$list = ((int)$list+$this->perpage);
			}
			$this->pages_list[$i]["page"] = $i;
			$this->pages_list[$i]["limit"] = $list;
		}

		foreach($this->pages_list as $val) {
			if($val["limit"] == $this->pagenow) {
				$this->current_page = $val["page"];
			}
		}
		$new_page_m  = (int)$this->current_page;
		$new_page_p  = (int)$this->current_page;
		(array)$new_pages[] = $this->current_page;
		(array)$this->last_page = array_pop($this->pages_list);

		/**
		 * fixes the counting for the first pages
		 **/
		if($this->current_page < 4) {
			if($this->current_page == 2) {
				$this->maxpages = $this->maxpages + 2;
			} elseif($this->current_page == 3) {
				$this->maxpages = $this->maxpages + 1;
			} elseif($this->current_page == 0) {
				$this->maxpages = $this->maxpages + 4;
			} else {
				$this->maxpages = $this->maxpages + 3;
			}
		}

		$limitpage = $this->pages - $this->maxpages;

		/**
		 * fixes the counting for the last pages pages
		 **/
		if($this->current_page >= $limitpage) {
			if($this->current_page == $this->pages) {
				$this->maxpages = $this->maxpages + 3;
			} elseif($this->current_page == ($this->pages-1)) {
				$this->maxpages = $this->maxpages + 2;
			} elseif($this->current_page == ($this->pages-2)) {
				$this->maxpages = $this->maxpages + 1;
			}
		}

		for($i=0; $i<$this->maxpages; $i++) {
			$new_page_m = $new_page_m - 1;
			$new_page_p = $new_page_p + 1;

			if($new_page_m >= 0) {
				$new_pages[] = $new_page_m;
			}
			if($new_page_p <= $this->last_page["page"]) {
				$new_pages[] = $new_page_p;
			}
		}

		sort($new_pages);

		(array)$this->new_pages_output = $new_pages;

		return TRUE;
	}

	/**
	 * create_pagination
	 * Creates the pagination and gives an array back
	 * output for the output and value for the value (haha)
	 * @return array
	 */
	public function create_pagination() {
		if($this->pagenow == 0) {
			$last_page_limit = 0;
		} else {
			$last_page_limit = (int)$this->pagenow - $this->perpage;
		}

		(array)$pagination = array();

		$pagination["first"]["value"] = 0;
		$pagination["first"]["output"] = $this->signs["first"];

		$pagination["previous"]["value"] = $last_page_limit;
		$pagination["previous"]["output"] = $this->signs["previous"];

		foreach($this->new_pages_output as $val) {
			if($val != 0) {
				if($val < $this->last_page["page"]) {
					if($this->pages_list[$val]["limit"] == $this->pagenow) {
						$pagination["next"][$val]["current"]["output"] = $val;
					} else {
						$pagination["next"][$val]["value"] = $this->pages_list[$val]["limit"];
						$pagination["next"][$val]["output"] = $val;
					}
				}
			}
		}

		$last_limit = (int)$this->last_page["limit"] - $this->perpage;
		if($this->pagenow == $last_limit) {
			$next_page_limit = $last_limit;
		} else {
			$next_page_limit = (int)$this->pagenow + $this->perpage;
		}

		$pagination["nextpage"]["value"] = $next_page_limit;
		$pagination["nextpage"]["output"] = $this->signs["nextpage"];

		$pagination["last"]["value"] = $last_limit;
		$pagination["last"]["output"] = $this->signs["last"];

		return (object)$pagination;
	}

	/**
	 * render_pagination
	 * @param  string $output switch mode (html direct output - var returns output)
	 * @param  string $extend extend your URL
	 * @return string $output output including the html code for the pagination
	 */
	public function render_pagination($output="html", $extend=null, $query=null) {
		switch(strtolower($output)) {
			case "html":
				if($this->pagenow == 0) {
					$last_page_limit = 0;
				} else {
					$last_page_limit = (int)$this->pagenow - $this->perpage;
				}

				echo '<div id="'.$this->classnames["pagination"].'">';
				echo '<div class="'.$this->classnames["class"].'" title="First Page" onclick="self.location.href=\''.($query?$query."&":'?').$this->method.'=0'.$extend.'\'">
							<a href="'.($query?$query."&":'?').$this->method.'=0'.$extend.'">'.$this->signs["first"].'</a>
					  </div> 
					  <div title="Previous Page" class="'.$this->classnames["class"].'" onclick="self.location.href=\''.($query?$query."&":'?').$this->method.'='.$last_page_limit.$extend.'\'">
					  	<a href="'.($query?$query."&":'?').$this->method.'='.$last_page_limit.$extend.'">'.$this->signs["previous"].'</a>
					  </div> ';

				foreach($this->new_pages_output as $val) {
					if($val != 0) {
						if($val < $this->last_page["page"]) {
							if($this->pages_list[$val]["limit"] == $this->pagenow) {
								echo '<div title="Current Page" class="'.$this->classnames["class"].' '.$this->classnames["active"].'">'.$val.'</div> ';
							} else {
								echo '<div title="Page: '.$val.'" class="'.$this->classnames["class"].'" onclick="self.location.href=\''.($query?$query."&":'?').$this->method.'='.$this->pages_list[$val]["limit"].$extend.'\'">
										<a href="'.($query?$query."&":'?').$this->method.'='.$this->pages_list[$val]["limit"].$extend.'">'.$val.'</a>
									  </div> ';
							}
						}
					}
				}
				$last_limit = (int)$this->last_page["limit"] - $this->perpage;
				if($this->pagenow == $last_limit) {
					$next_page_limit = $last_limit;
				} else {
					$next_page_limit = (int)$this->pagenow + $this->perpage;
				}

				echo ' <div class="'.$this->classnames["class"].'"  title="Next Page" onclick="self.location.href=\''.($query?$query."&":'?').$this->method.'='.$next_page_limit.$extend.'\'">
					   		<a href="'.($query?$query."&":'?').$this->method.'='.$next_page_limit.$extend.'">'.$this->signs["nextpage"].'</a>
					   </div> <div class="'.$this->classnames["class"].'" title="Last Page" onclick="self.location.href=\''.($query?$query."&":'?').$this->method.'='.$last_limit.$extend.'\'">
					   		<a href="'.($query?$query."&":'?').$this->method.'='.$last_limit.$extend.'">'.$this->signs["last"].'</a>
					   </div>';
				echo "<br style=\"clear: left;\" /></div>";
			break;
			case "var":
				if($this->pagenow == 0) {
					$last_page_limit = 0;
				} else {
					$last_page_limit = (int)$this->pagenow - $this->perpage;
				}


				$this->output .= '<div id="'.$this->classnames["pagination"].'">';
				$this->output .= '<div class="'.$this->classnames["class"].'" ng-click="loadPages(0)">
									'.$this->signs["first"].'
								  </div>
								  <div class="'.$this->classnames["class"].'" ng-click="loadPages('.$last_page_limit.')">
								  	'.$this->signs["previous"].'
								  </div> ';

				foreach($this->new_pages_output as $val) {
					if($val != 0) {
						if($val < $this->last_page["page"]) {
							if($this->pages_list[$val]["limit"] == $this->pagenow) {
								$this->output .= '<div class="'.$this->classnames["class"].' '.$this->classnames["active"].'">'.$val.'</div> ';
							} else {
								$this->output .= '<div class="'.$this->classnames["class"].'" ng-click="loadPages('.$this->pages_list[$val]["limit"].')">
													'.$val.'
												  </div> ';
							}
						}
					}
				}
				$last_limit = (int)$this->last_page["limit"] - $this->perpage;
				if($this->pagenow == $last_limit) {
					$next_page_limit = $last_limit;
				} else {
					$next_page_limit = (int)$this->pagenow + $this->perpage;
				}

				$this->output .= ' <div class="'.$this->classnames["class"].'" ng-click="loadPages('.$next_page_limit.')">
										'.$this->signs["nextpage"].'
								   </div>
								   <div class="'.$this->classnames["class"].'" ng-click="loadPages('.$last_limit.')">
								   		'.$this->signs["last"].'
								   </div>';
				$this->output .=  "<br style=\"clear: left;\" /></div>";

				return $this->output;
			break;
			default:
				return "Invalid method";
			break;
		}
	}
}
