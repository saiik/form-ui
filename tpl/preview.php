<div id="overlay">
	<div id="formBuilder" style="margin-top: 70px;">
		<div id="formBuilderLeft">
			<div class="formWidgetContainer">
				<div class="formWidgetContainerHead">
					Form Details
				</div>
				<div class="formWidgetContainerContent">
					<div class="col-xs-6 col-md-4">
						Job ID:
					</div>
					<div class="col-xs-6 col-md-4">
						<input type="text" ng-model="job.id" class="form-ui-input" ng-class="{alertForm: alert}"/>
					</div>
					<br style="clear: left;" />
					<br />
					<button class="form-ui-button" ng-click="previewForm(job.id)"><span class="glyphicon glyphicon-eye-open"></span> Preview Job</button>
					<button class="form-ui-button" onclick="self.location.href='#new'"><span class="glyphicon glyphicon-pencil"></span> Edit Form</button>
					<br style="clear: left;" />
				</div>
			</div>
			<div ng-if="jobs" class="formWidgetContainer">
				<div class="formWidgetContainerHead">
					Previously searched jobs
				</div>
				<div class="formWidgetContainerContent">
					<span ng-repeat="job in jobs">
						Job: <a ng-click="previewForm(job.job_id)" href>{{job.job_id}}</a>
						<br />
					</span>
				</div>
			</div>
		</div>
		<div id="formBuilderRight">
			<div id="formBuildGridPreview">
				<div style="width: 300px; margin-left: auto; margin-right: auto; padding-top: 50%;">
					<div class="progress">
					  	<div class="progress-bar progress-bar-success progress-bar-striped active"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							<span><strong>Loading form</strong></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
