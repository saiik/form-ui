<div class="windowBoxWrapper">
	<div class="windowBoxHead">
		Insert a Variable
	</div>
	<div class="windowBoxContent">
		<p class="windowBoxContentHtml">
			Variable (required):<br />
			<select name="variableDropdown" ng-model="input">
				<option ng-repeat="var in variableList.response" value="{{var}}">{{var}}</option>
			</select>
		</p>
		<br />
		<div class="windowBoxButtons">
			<button ng-click="closeWindow()">Close</button> <button ng-click="saveVariable(input)">Save</button>
		</div>
	</div>
</div>
