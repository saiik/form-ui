<div class="windowBoxWrapper">
	<div class="windowBoxHead">
		Insert an Table
	</div>
	<div class="windowBoxContent">
		<label><input ng-disabled="alreadyCreated" type="radio" name="table" ng-click="customeTableView = false"> Sub-Asset</label>
		<label><input type="radio" name="table" ng-click="customeTableView = true" checked> Custom</label>
		<div ng-if="customeTableView === false">
			Create a Sub-Assets Table<br /><br />
			Select up to 7 collums:<br />
			<p>
				<div class="col-md-8">
					Status
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-model="asset.status" ng-disabled="!asset.status && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Model
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-model="asset.model" ng-disabled="!asset.model && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Serial Number
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-model="asset.serial" ng-disabled="!asset.serial && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Notes
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-model="asset.notes" ng-disabled="!asset.notes && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Asset Name
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-model="asset.name" ng-disabled="!asset.name && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Asset Description
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-model="asset.desc" ng-disabled="!asset.desc && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Location
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-model="asset.loca" ng-disabled="!asset.loca && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Input field
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-model="asset.input" ng-disabled="!asset.input && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Checkbox field
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-model="asset.check" ng-disabled="!asset.check && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<br /><br />
			<div class="windowBoxButtons">
				<button ng-click="closeWindow()">Close</button> <button ng-click="createAssetTable(asset)">Create</button>
			</div>
		</div>
		<div ng-if="customeTableView === true">
			<p class="windowBoxContentHtml">
				Table Name (required):<br />
				<input type="text" ng-model="input.name" ng-enter="saveTable(input)" class="form-ui-input" /><br />
				Row amount (required):<br />
				<input type="text" ng-model="input.rows" ng-enter="saveTable(input)" class="form-ui-input" /><br />
				Col amount (required):<br />
				<input type="text" ng-model="input.cols" ng-enter="saveTable(input)" class="form-ui-input" />
			</p>
			<br />
			<div class="windowBoxButtons">
				<button ng-click="closeWindow()">Close</button> <button ng-click="saveTable(input)">Save</button>
			</div>
		</div>
	</div>
</div>
