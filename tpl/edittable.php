<div class="windowBoxWrapper">
	<div class="windowBoxHead">
		Customize the Table
	</div>
	<div class="windowBoxContent">
		Table Name: <input type="text" ng-model="tableName" ng-enter="saveEditTable(tableName)" class="form-ui-input" />
		<hr />
		<div class="windowBoxButtons">
			<button ng-click="closeWindow()">Close</button> <button ng-click="saveEditTable(tableName)">Save</button>
		</div>
	</div>
</div>
