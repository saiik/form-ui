<div class="windowBoxWrapper">
	<div class="windowBoxHead">
		Insert an Table
	</div>
	<div class="windowBoxContent">
		<div>
			Update the Sub-Assets Table<br /><br />
			Select up to 7 collums:<br />
			<p>
				<div class="col-md-8">
					Status
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-checked="assetsCheck.status" ng-model="assetsNeu.status" ng-disabled="!asset.status && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Model
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-checked="assetsCheck.model" ng-model="assetsNeu.model" ng-disabled="!asset.model && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Serial Number
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-checked="assetsCheck.serial" ng-model="assetsNeu.serial" ng-disabled="!asset.serial && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Notes
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-checked="assetsCheck.notes" ng-model="assetsNeu.notes" ng-disabled="!asset.notes && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Asset Name
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-checked="assetsCheck.name" ng-model="assetsNeu.name" ng-disabled="!asset.name && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Asset Description
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-checked="assetsCheck.desc" ng-model="assetsNeu.desc" ng-disabled="!asset.desc && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Location
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-checked="assetsCheck.loca" ng-model="assetsNeu.loca" ng-disabled="!asset.loca && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Input field
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-checked="assetsCheck.input" ng-model="assetsNeu.input" ng-disabled="!asset.input && maxAssets()" />
				</div>
				<br style="clear: left;" />
				<div class="col-md-8">
					Checkbox field
				</div>
				<div class="col-md-4">
					<input type="checkbox" ng-checked="assetsCheck.check" ng-model="assetsNeu.check" ng-disabled="!asset.check && maxAssets()" />
				</div>
				<br style="clear: left;" />
			</p>
			<div class="windowBoxButtons">
				<button ng-click="closeWindow()">Close</button> <button ng-click="updateAssetTable(assetsNeu)">Save</button>
			</div>
		</div>
	</div>
</div>
