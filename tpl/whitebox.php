<div class="windowBoxWrapper" style="margin-top: -50px;">
	<div class="windowBoxHead">
		Insert Content
	</div>
	<div class="windowBoxContent">
		<p class="windowBoxContentHtml">
			Content:<br /><br />
			<textarea ng-model="content" class="form-ui-input" cols="70" rows="5"></textarea>
		</p>
		Use <code>[text="NAME"]</code> for an text field and <code>[checkbox="NAME"]</code> for a checkbox!
		<br /><br />
		Current available variables:<br />
		<code>
			<span ng-repeat="vars in variableList.response | partition:5"> <span ng-repeat="var in vars"> {{var}} </span> <br /></span>
		</code>
		<br /><br />
		<div class="windowBoxButtons">
			<button ng-click="closeWindow()">Close</button> <button ng-click="saveContent(content)">Save</button>
		</div>
	</div>
</div>
