<div class="windowBoxWrapper">
	<div class="windowBoxHead">
		Insert an Input field
	</div>
	<div class="windowBoxContent">
		<p class="windowBoxContentHtml">
			Select a Input type:<br /><br />
			<label ng-click="createDrop = false"><input type="radio" name="input" ng-model="input.check" value="text"> Text</label><br />
			<label ng-click="createDrop = false"><input type="radio" name="input" ng-model="input.check" value="check"> Checkbox</label><br />
			<label ng-click="createDrop = false"><input type="radio" name="input" ng-model="input.check" value="radio"> Radiobox</label><br />
			<label ng-click="createDrop = true"><input type="radio" name="input" ng-model="input.check" value="drop"> Dropdown</label>
		</p>
		Label:<br />
		<input type="text" ng-model="input.label" ng-enter="saveInputType(input)" class="form-ui-input" /><br />
		<span ng-if="!createDrop">Value:<br />
		<input type="text" ng-model="input.value" ng-enter="saveInputType(input)" class="form-ui-input" /><br /></span><br />
		<div class="windowBoxButtons">
			<button ng-click="closeWindow()">Close</button> <button ng-click="saveInputType(input)">Save</button>
		</div>
	</div>
</div>
