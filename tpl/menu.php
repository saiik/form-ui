<div id="overlay">
	<div id="overlayContainer" style="z-index: 999;">
		<div id="overlayHead">
			Choose a form
		</div>
		<div id="overlayContent">
			<div ng-if="load">
				<div ng-if="noforms">
					<br />No forms available - Create a new one
				</div>
				<div ng-if="!onLoad">
					<ul class="list-group" style="box-shadow: none;">
						<div ng-repeat="forms in formLoad">
							<div ng-show="forms.response.data.rows.restore">
								<center><a href ng-class="{restoreClass :!restore}" ng-click="restore = false">Load</a> - <a href ng-class="{restoreClass :restore}" ng-click="restore = true">Restore</a></center>
							</div>
							<div ng-if="forms.response.status.code === 406">
								<br />No forms available - Create a new one
							</div>
							<div ng-repeat="form in forms.response.data.rows.rows" ng-if="!restore">
								<a href ng-click="getForm(form.form_id)"><li class="list-group-item" ng-if="form" style="border: none; border-bottom: 1px #eee solid;">{{form.form_name}}</li></a>
							</div>
							<div ng-repeat="form in forms.response.data.rows.restore" ng-if="restore">
								<a href ng-click="getForm(form.form_id, restore)">
									<li class="list-group-item" ng-if="form" style="border: none; border-bottom: 1px #eee solid;">
										<div class="col-md-6">
											{{form.form_name}}
										</div> 
										<div class="col-md-6" stly="font-size: 8px;">
											<small>{{form.form_date * 1000 | date:'yy/MM/dd HH:mm'}}</small>
										</div>
										<br style="clear: left;" />
									</li>
								</a>
							</div>
						</div>
					</ul>
					<div ng-repeat="pagination in formLoad">
						<div compile="pagination.response.data.pagination"></div>
					</div>
				</div>
				<div ng-if="onLoad">
					<br />
					<div class="progress">
					  <div class="progress-bar progress-bar-success progress-bar-striped active"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
					    <span><strong>Loading forms</strong></span>
					  </div>
					</div>
				</div>
				<hr />
				<div style="float: left;">
					<a href="#new" ng-click="newForm()"><span class="glyphicon glyphicon-file"></span> Create a new Form</a>
				</div>
				<div style="float: right;">
					<small><a href="#new">Close <span class="glyphicon glyphicon-remove"></span></a></small>
				</div>
				<br style="clear: all;" />
			</div>
			<div ng-if="!load">
				<br />
				<div class="progress">
				  	<div class="progress-bar progress-bar-success progress-bar-striped active"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						<span><strong>Loading form</strong></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
