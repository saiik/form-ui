<div class="windowBoxWrapper" style="width: 500px; margin-left: -100px;">
	<div class="windowBoxHead">
		Insert an Image
	</div>
	<div class="windowBoxContent">
		<p class="windowBoxContentHtml">
	        <input type="file" nv-file-select uploader="uploader"/><br/>
	        <ul id="fUpload" class="list-unstyled">
	            <li ng-repeat="item in uploader.queue">
	                <div class="col-md-8">
	                	Name: <span ng-bind="item.file.name"></span><br/>
	                </div>
	                <div class="col-md-4">
	                	<button type="button" class="btn btn-success btn-xs" ng-click="item.upload()">upload</button>
	            	</div>
	            	<br style="clear: left;" />
	            </li>
	        </ul>
			<div id="availablePictures" ng-if="pictures">
				<hr />
				<h6>Current Pictures for this form:</h6>
				<br />
				<div ng-repeat="pic in pictures" style="float: left; margin-right: 10px; margin-bottom: 10px; width: 60px; height: 50px; position: relative;" class="imgBoxImg" ng-if="pic.thumb">
					<div class="deleteImage" ng-click="deleteImage(pic.thumb)" title="Delete Image">
						<span class="glyphicon glyphicon-remove"></span>
					</div>
					<img ng-src="{{pic.thumb}}" ng-click="createImage(pic.thumb)" title="Add Image"/>
				</div>
				<br style="clear: left;" />
				<hr />
			</div>
			<div ng-if="!pictures">
				<hr />
				<h6>No pictures available, start uploading some!</h6>
				<hr />
			</div>
		</p>
		<div class="windowBoxButtons">
			<button ng-click="closeWindow()">Close</button>
		</div>
	</div>
</div>
