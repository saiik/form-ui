<div class="windowBoxWrapper">
	<div class="windowBoxHead">
		Customize the DropDown
	</div>
	<div class="windowBoxContent">
		Current fields:<br /><br />
		<span ng-repeat="drops in dropdown | partition:2">
			<span ng-repeat="drop in drops">
				<span ng-repeat="(key, value) in drop">
					<div class="col-xs-6 col-md-4">
						{{value}}
					</div>
					<div ng-click="deleteDrop(key)" class="col-md-1" style="font-size: 12px; color: red; cursor: pointer;">
						<span class="glyphicon glyphicon-remove"></span>
					</div>
				</span>
			</span>
			<br />
		</span>
		<hr />
		<input type="text" ng-model="drop" style="width: 120px;" ng-enter="addDrop(drop); drop = ''" class="form-ui-input" /> <span ng-click="addDrop(drop); drop = ''" class="btn btn-success" style="padding: 2px 12px;">Add</span>
		<hr />
		<div class="windowBoxButtons">
			<button ng-click="closeWindow()">Close</button> <button ng-click="saveDropdown()">Save</button>
		</div>
	</div>
</div>
